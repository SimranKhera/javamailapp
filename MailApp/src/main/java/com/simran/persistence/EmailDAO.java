package com.simran.persistence;

import com.simran.beans.*;
import com.simran.exceptions.MovingEmailFromDraftFolderException;
import com.simran.fxbeans.EmailTableFXBean;
import com.simran.fxbeans.FolderTreeDataBean;
import java.io.File;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;

/**
 * This is an interface for CRUD methods to manipulate emails
 *
 * @author Simranjot Kaur Khera
 */
public interface EmailDAO {

    //Create
    public int createEmail(EmailDataBean emailData) throws SQLException;

    public int createFolder(String folderName) throws SQLException;

    public int createInboxEmails(ReceivedEmail[] receivedEmails) throws SQLException;

    //Read
    public List<EmailDataBean> findEmailsInFolder(String folderName) throws SQLException;

    public EmailDataBean findByID(int id) throws SQLException;
    
    public List<FolderTreeDataBean> findAllFolders() throws SQLException;
    
    public int findFolderByName(String folderName) throws SQLException;
    
    
    //Update
    public int updateFolder(int emailID, String folderName) throws SQLException, MovingEmailFromDraftFolderException;
    
    //Delete 
    public int deleteEmail(int emailID) throws SQLException;

    public String getMessageFromType(List<EmailMessage> messages, String texthtml);

    
}
