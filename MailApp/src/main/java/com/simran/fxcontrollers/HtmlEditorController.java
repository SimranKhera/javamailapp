package com.simran.fxcontrollers;

import com.simran.beans.EmailDataBean;
import com.simran.business.PropertiesManager;
import com.simran.business.SendAndReceive;
import com.simran.exceptions.InvalidBCCAddressException;
import com.simran.exceptions.InvalidCCAddressException;
import com.simran.exceptions.InvalidMailConfigBeanException;
import com.simran.exceptions.InvalidToAddressException;
import com.simran.exceptions.NoEmailsProvidedException;
import com.simran.fxbeans.EmailHtmlEditorFXBean;
import com.simran.fxbeans.EmailTableFXBean;
import com.simran.fxbeans.MailConfigBean;
import com.simran.persistence.EmailDAO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;
import javafx.beans.binding.Bindings;
import javax.activation.DataSource;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides means to see the contents of an email on a HTML editor as
 * well as write emails to send and receive them. It supports displaying an
 * email on the HTML editor when an email is clicked in the Email TableView.
 *
 * @author Simranjot Kaur Khera
 */
public class HtmlEditorController {

    private final static Logger LOG = LoggerFactory.getLogger(HtmlEditorController.class);

    private EmailHtmlEditorFXBean emailHtmlEditorBean;

    private EmailTableController tableController;

    private RootLayoutController rootController;

    private EmailDAO emailDAO;

    private MailConfigBean mailConfigBean = new MailConfigBean();

    private PropertiesManager propManager = new PropertiesManager();

    private SendAndReceive sendAndReceive = new SendAndReceive();

    private List<File> regAttachments = new ArrayList<File>();
    
    private List<File> foundAttachments = new ArrayList<File>();

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="emailHtmlLayout"
    private BorderPane emailHtmlLayout; // Value injected by FXMLLoader

    @FXML // fx:id="toTextField"
    private TextField toTextField; // Value injected by FXMLLoader

    @FXML // fx:id="ccTextField"
    private TextField ccTextField; // Value injected by FXMLLoader

    @FXML // fx:id="bccTextField"
    private TextField bccTextField; // Value injected by FXMLLoader

    @FXML // fx:id="subjectTextField"
    private TextField subjectTextField; // Value injected by FXMLLoader

    @FXML // fx:id="htmlEditor"
    private HTMLEditor htmlEditor; // Value injected by FXMLLoader

    public List<File> getFoundAttachments(){
        return this.foundAttachments;
    }
    
    /**
     * Called when the email is sent It gets data necessary to create an email,
     * sends the email and then stores it in the database. It then updates the
     * inbox folder by receiving emails. It then clears the editor and shows the
     * INBOX folder.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void onSendClicked(ActionEvent event) throws IOException, SQLException {

        this.emailHtmlEditorBean.setHtmlMessage(htmlEditor.getHtmlText());
        LOG.info("Email Being sent " + this.emailHtmlEditorBean.toString());

        this.sendAndStoreEmail();

        //Wait before receiving emails.
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOG.error("Threading error", e);
        }

        //Receive emails, and store them in database. 
        try {
            //Storing received emails into database. 
            this.emailDAO.createInboxEmails(this.sendAndReceive.receiveEmail(mailConfigBean));
        } catch (InvalidMailConfigBeanException e) {
            this.rootController.errorAlert("Problem with mailConfig Bean");
        }

        this.clearEditor();
        //show inbox emails after receiving emails
        this.tableController.displayTheTable("INBOX");
    }

    /**
     * Called when email is about to be saved. Creates an emailDataBean object
     * and create a new email. If the draft had already been previously saved,
     * the email will be deleted by the emailDAO and then created again.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void onSaveClicked(ActionEvent event) throws IOException {
        LOG.info("Save clicked - About to save draft");
        //Get all addresses on the gui and attachments
        doBindings();
        List<String> toAddresses = getAddressesFromString(this.emailHtmlEditorBean.getTo());
        List<String> ccAddresses = getAddressesFromString(this.emailHtmlEditorBean.getCc());
        List<String> bccAddresses = getAddressesFromString(this.emailHtmlEditorBean.getBcc());

        //Getting subject and email message. 
        String subject = this.emailHtmlEditorBean.getSubject();
        String htmlMessage = this.emailHtmlEditorBean.getHtmlMessage();

        try {
            //creating object for creation of email
            EmailDataBean emailToSave = this.createEmailDataBean(bccAddresses, ccAddresses, toAddresses, "DRAFT", subject, htmlMessage);
            this.emailDAO.createEmail(emailToSave);
            this.tableController.displayTheTable("DRAFT");
        } catch (SQLException ex) {
            LOG.error("SQLException while saving draft", ex);
        }

    }

    /**
     * Show the clicked email on the editor. First finds the email on the
     * database and displays its information. Depending on the type of email, it
     * makes it editable or not editable.
     *
     * @param tableBean
     */
    public void showEmailOnEditor(EmailTableFXBean tableBean) {
        LOG.info("Showing email Clicked");
        if (tableBean == null) {
            LOG.info("table bean is null");
            return;
        }
        try {
            EmailDataBean clickedEmail = this.emailDAO.findByID(tableBean.getEmailID());
            LOG.info("Clicked email is: " + clickedEmail.toString());
            fillHtmlEditorBean(clickedEmail);

            if (clickedEmail.getFolderid() == this.emailDAO.findFolderByName("DRAFT")) {
                setEditable(true);
            } else {
                setEditable(false);
            }
        } catch (SQLException ex) {
            LOG.error("SQLException while retrieving draft folder id", ex);
        }

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert emailHtmlLayout != null : "fx:id=\"emailHtmlLayout\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert toTextField != null : "fx:id=\"toTextField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert ccTextField != null : "fx:id=\"ccTextField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert bccTextField != null : "fx:id=\"bccTextField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert subjectTextField != null : "fx:id=\"subjectTextField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert htmlEditor != null : "fx:id=\"htmlEditor\" was not injected: check your FXML file 'HtmlEditor.fxml'.";

        this.emailHtmlEditorBean = new EmailHtmlEditorFXBean();
        //Loadding mailconfigbean and using it to send the email. 
        try {
            this.propManager.loadTextProperties(this.mailConfigBean, "", "MailConfig");
        } catch (IOException ex) {
            LOG.error("Error loading mailconfig - file not found", ex);
        }

        doBindings();
    }

    /**
     * Binds all fields for to, cc,bcc and subject to the bean properties.
     * Changes in the bean will reflect on the GUI and vice versa.
     *
     * EmailDataBean must not be null --> nullpointer otherwise
     */
    private void doBindings() {
        LOG.info("doing bindings");
        Bindings.bindBidirectional(toTextField.textProperty(), emailHtmlEditorBean.toProperty());
        Bindings.bindBidirectional(ccTextField.textProperty(), emailHtmlEditorBean.ccProperty());
        Bindings.bindBidirectional(bccTextField.textProperty(), emailHtmlEditorBean.bccProperty());
        Bindings.bindBidirectional(subjectTextField.textProperty(), emailHtmlEditorBean.subjectProperty());
    }

    /**
     * Gets a reference to a EmailDAO in order to make changes to the database.
     * This will be used to save emails in the database as well send and receive
     * them.
     *
     * @param emailDAO
     */
    public void setEmailDAO(EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    /**
     * Gets reference to table controller.
     *
     * @param tableController
     */
    public void setEmailTableController(EmailTableController tableController) {
        this.tableController = tableController;
    }

    /**
     * Gets reference to root controller (used to display error dialogs)
     *
     * @param rootController
     */
    public void setRootController(RootLayoutController rootController) {
        this.rootController = rootController;
    }

    /**
     * Fills The html editor bean with email data. When the htmlBean is filled
     * the changed are automatically shown on gui.
     *
     * @param emailDataBean
     */
    private void fillHtmlEditorBean(EmailDataBean emailDataBean) {
        LOG.info("Filling htmlEditorBean");
        this.emailHtmlEditorBean.setBcc(getAddressesAsString(emailDataBean.email.bcc()));
        this.emailHtmlEditorBean.setCc(getAddressesAsString(emailDataBean.email.cc()));
        this.emailHtmlEditorBean.setTo(getAddressesAsString(emailDataBean.email.to()));
        this.emailHtmlEditorBean.setSubject(emailDataBean.email.subject());

        this.emailHtmlEditorBean.setHtmlMessage(this.emailDAO.getMessageFromType(emailDataBean.email.messages(), "text/html"));
        //HtmlEditor has no text property to bind to, must set text explicitly.
        StringBuilder htmlMessage = new StringBuilder(this.emailHtmlEditorBean.getHtmlMessage());
        
        this.foundAttachments.clear();
        //Get embedded attachments and display them. 
        List<EmailAttachment<? extends DataSource>> embattachments = emailDataBean.email.attachments();
        for (EmailAttachment<? extends DataSource> attachment : embattachments) {
            LOG.info("Attachment found: " + attachment.getName());
            if (attachment.isEmbedded()) {
                //Temporary file created to be able to get the full URL of the attachment file. 
                File tempFile = new File(attachment.getName());
                this.foundAttachments.add(tempFile);
                htmlMessage.append("<img src='" + tempFile.toURI() + "'/>");
            }

        }
        this.htmlEditor.setHtmlText(htmlMessage.toString());
    }

    /**
     * Creates emailDataBean from given lists of emails, subject and
     * htmlMessage. These lists contain information entered on the gui. This
     * method can be used for saving sent and draft emails -> specified by the
     * folderName parameter. This is not necessary for received emails since it
     * is done in the emailDAO methods.
     *
     * @param regAttachments
     * @param embAttachments
     * @param bccAddresses
     * @param ccAddresses
     * @param toAddresses
     * @param emailFolderName
     * @param subject
     * @param htmlMessage
     * @return
     * @throws SQLException
     */
    private EmailDataBean createEmailDataBean(List<String> bccAddresses, List<String> ccAddresses, List<String> toAddresses, String emailFolderName, String subject, String htmlMessage) throws SQLException {
        LOG.info("Creating data bean object");
        EmailDataBean emailDataBean = new EmailDataBean();
        Email emailObject = new Email();

        //Setting email data object
        emailDataBean.setFolderid(this.emailDAO.findFolderByName(emailFolderName));
        emailObject.subject(subject);
        emailObject.from(this.mailConfigBean.getEmailAddress());
        emailObject.htmlMessage(htmlMessage);

        String[] bccAddressesStrings = new String[bccAddresses.size()];
        bccAddressesStrings = bccAddresses.toArray(new String[0]);

        String[] ccAddressesStrings = new String[ccAddresses.size()];
        ccAddressesStrings = ccAddresses.toArray(new String[0]);
        String[] toAddressesStrings = new String[toAddresses.size()];
        toAddressesStrings = toAddresses.toArray(new String[0]);

        LOG.info("TOO EMAILS: " + toAddressesStrings.toString());
        emailObject.bcc(bccAddressesStrings);
        emailObject.cc(ccAddressesStrings);
        emailObject.to(toAddressesStrings);

        if (emailFolderName != "DRAFT") {
            emailObject.sentDate(new Date());
        }

        emailDataBean.setEmail(emailObject);
        LOG.info("email bean created: " + emailDataBean.toString());
        return emailDataBean;
    }

    /**
     * Sets the htmlEditor components to be editable or not depending on the
     * boolean given.
     *
     * @param editable
     */
    private void setEditable(Boolean editable) {
        LOG.info("Setting editable to " + editable);
        this.bccTextField.setEditable(editable);
        this.toTextField.setEditable(editable);
        this.ccTextField.setEditable(editable);
        this.subjectTextField.setEditable(editable);
        this.htmlEditor.setDisable(!editable);
    }

    /**
     * Since there can be many bcc, to and cc addresses in an email, to display
     * the emails in the gui editor, we create a single string separated by
     * semi-colons and a space to display all the emails in one text field. This
     * is used when we want to convert a EmailDataBean object to a htmlEditor
     * object.
     *
     * @param addresses
     * @return String with all addresses
     */
    private String getAddressesAsString(EmailAddress[] addresses) {
        StringBuilder stringBuilder = new StringBuilder();
        for (EmailAddress address : addresses) {
            stringBuilder.append(address.getEmail());
            stringBuilder.append("; ");
        }
        return stringBuilder.toString();
    }

    /**
     * Splits the addressString by ";" and get each address as List. This is
     * used to get all emails entered in to, cc, and bcc textfields.
     *
     * @param addressString
     * @return List of addresses.
     */
    private List<String> getAddressesFromString(String addressString) {
        String[] addresses = addressString.split(";");

        List<String> emailAddresses = new ArrayList<String>();

        for (String address : addresses) {
            if (!address.isBlank()) {
                emailAddresses.add(address.trim());
            }
        }
        LOG.info("Emails obtained from TextField: " + emailAddresses.toString());
        return emailAddresses;
    }

    /**
     * This method clears the contents of the email html Editor and table. Table
     * must be cleared once the email is sent.
     */
    public void clearEditor() {
        //Emptying the htmlEditor and email fields after the email is sent on GUI.
        this.emailHtmlEditorBean = new EmailHtmlEditorFXBean();

        //binding is necessary to give the bind the new reference
        this.doBindings();
        this.htmlEditor.setHtmlText("");
        this.setEditable(true);
    }

    /**
     * Sends and stores the new email in database. 1.First gets all the
     * necessary data for the email object. 2.Loads the mailconfigbean with data
     * in mailconfig properties file 3. Sends email and stores it in database.
     *
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException
     * @throws IOException
     * @throws IOException
     * @throws SQLException
     */
    private void sendAndStoreEmail() throws IOException, SQLException {
        LOG.info("Send And Storing Email from editor");
        //Get all addresses on the gui and attachments
        List<String> toAddresses = getAddressesFromString(this.emailHtmlEditorBean.getTo());
        List<String> ccAddresses = getAddressesFromString(this.emailHtmlEditorBean.getCc());
        List<String> bccAddresses = getAddressesFromString(this.emailHtmlEditorBean.getBcc());
        List<File> regAttachments = new ArrayList<File>();
        List<File> embAttachments = new ArrayList<File>();

        //Getting subject and email message. 
        String subject = this.emailHtmlEditorBean.getSubject();
        String htmlMessage = this.emailHtmlEditorBean.getHtmlMessage();

        Email email = this.sendEmail(toAddresses, ccAddresses, bccAddresses, subject, "", htmlMessage, embAttachments);

        //Store email in database
        //EmailDataBean emailDataBean = this.createEmailDataBean(bccAddresses, ccAddresses, toAddresses, "SENT", subject, htmlMessage);
        EmailDataBean emailDataBean = new EmailDataBean();
        emailDataBean.setEmail(email);
        
        emailDataBean.setFolderid(this.emailDAO.findFolderByName("SENT"));
        
        int gen = this.emailDAO.createEmail(emailDataBean);
        
        LOG.info("CREATED EMAIL: " + this.emailDAO.findByID(gen).toString());
        this.foundAttachments.clear();
        this.regAttachments.clear();
    }

    /**
     * Sends an email with given fields and catches all exceptions to display to
     * user.
     *
     * @param toAddresses
     * @param ccAddresses
     * @param bccAddresses
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param regAttachments
     * @param embAttachments
     */
    private Email sendEmail(List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String subject, String textMessage, String htmlMessage, List<File> embAttachments) {
        Email email = new Email();
        try {
            email = this.sendAndReceive.sendEmail(toAddresses, ccAddresses, bccAddresses, subject, textMessage, htmlMessage, this.regAttachments, embAttachments, this.mailConfigBean);
        } catch (InvalidToAddressException e) {
            this.rootController.errorAlert("Invalid To Address");
        } catch (InvalidCCAddressException e) {
            this.rootController.errorAlert("Invalid CC Address");
        } catch (InvalidBCCAddressException e) {
            this.rootController.errorAlert("Invalid BCC Address");
        } catch (NoEmailsProvidedException e) {
            this.rootController.errorAlert("Need an email to send to");
        } catch (InvalidMailConfigBeanException e) {
            this.rootController.errorAlert("Problem in config Bean");
        }
        return email;
    }
    /**
     * Adding the attachment to be sent with the email to a list. 
     * When email is being sent, the list will be used to send the email. 
     * @param attachment 
     */
    public void setAttachment(File attachment) {
        this.regAttachments.add(attachment);
    }
}
