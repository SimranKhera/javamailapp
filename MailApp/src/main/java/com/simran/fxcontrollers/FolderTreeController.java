package com.simran.fxcontrollers;

import com.simran.exceptions.MovingEmailFromDraftFolderException;
import com.simran.fxbeans.FolderTreeDataBean;
import com.simran.persistence.EmailDAO;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This controller uses the FolderTreeDataBean in order to display the name of
 * each folder in a Tree form. It supports emails being dragging into the tree.
 *
 * @author Simranjot Kaur Khera
 */
public class FolderTreeController {

    private EmailDAO emailDAO;

    private EmailTableController emailTableController;
    
    private RootLayoutController rootLayoutController;

    private final static Logger LOG = LoggerFactory.getLogger(FolderTreeController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="folderTreeView"
    private TreeView<FolderTreeDataBean> folderTreeView; // Value injected by FXMLLoader

    /**
     * Called when the email has been dropped into a folder
     * Calls updateFolder to update the folder to the given folder name of the email.
     * @param event
     */
    @FXML
    void onDragDroppedIntoTree(DragEvent event) throws SQLException {
        String folderName = getFolderNameFromString(event.getTarget().toString());
        LOG.info("Folder Dropped on " + folderName);

        
        Dragboard dragBoard = event.getDragboard();
        boolean success = false;

        if (dragBoard.hasString()) {
            LOG.info("FOUND FROM DRAG EMAILID " + dragBoard.getString());
            
            try{
                this.emailDAO.updateFolder(Integer.parseInt(dragBoard.getString()), folderName);
            }catch(MovingEmailFromDraftFolderException e){
                this.rootLayoutController.errorAlert("Cannot move a Draft email");
            }
        }
        event.setDropCompleted(success);
        event.consume();
    }

    /**
     * Drag over tree event accepts any transfer mode.
     *
     * @param event
     */
    @FXML
    void onDragOverFolderTree(DragEvent event) {
        if (event.getGestureSource() != event.getTarget()
                && event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.ANY);
        }

        event.consume();

    }
    /**
     * When a folder is clicked, the contents of the folder is shown on the table. 
     * Retrieves all the emails in the folder. 
     * @param event
     * @throws SQLException 
     */
    @FXML
    void onMouseClickedInTree(MouseEvent event) throws SQLException {
        String folderName = this.folderTreeView.getSelectionModel().getSelectedItem().getValue().getName();
        
        LOG.info("Folder Clicked : " + folderName);
        emailTableController.displayTheTable(folderName);
        
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        LOG.info("initialize");
        assert folderTreeView != null : "fx:id=\"folderTreeView\" was not injected: check your FXML file 'FolderTree.fxml'.";

        //setting root folder
        FolderTreeDataBean rootBean = new FolderTreeDataBean();

        //this sets the root of all folders on the tree
        rootBean.setName(resources.getString("Folder"));
        this.folderTreeView.setRoot(new TreeItem<>(rootBean));

        //Sets each element in the tree with a name from the provided foldertreebean
        //specifies the FolderTreeDataBean property to be used as folder name. 
        folderTreeView.setCellFactory((e) -> new TreeCell<FolderTreeDataBean>() {
            @Override
            protected void updateItem(FolderTreeDataBean item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.getName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });
    }

    /**
     * Getting reference from rootlayoutcontroller to the emailDAO
     *
     * @param emailDAO
     */
    public void setEmailDAO(EmailDAO emailDAO) {
        LOG.info("Setting emailDAO");
        this.emailDAO = emailDAO;
    }
    /**
     * Getting reference of RootLayoutController for displaying error message
     * @param rootLayoutController 
     */
    public void setRootLayout(RootLayoutController rootLayoutController){
        this.rootLayoutController= rootLayoutController;
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * EmailTableController from which it can request a reference to the
     * TreeView.
     *
     * @param emailTableController
     */
    public void setTableController(EmailTableController emailTableController) {
        this.emailTableController = emailTableController;
    }

    /**
     * Build the tree from the database (or fake data)
     * Gets all folders from database and displays them. 
     * @throws SQLException
     */
    public void displayTree() throws SQLException {
        LOG.info("Adding folders to root and displaying");
        List<FolderTreeDataBean> folderList = this.emailDAO.findAllFolders();
        ObservableList<FolderTreeDataBean> folderBeans = FXCollections.observableList(folderList);

        // Build an item for each folder and add it to the root
        if (folderBeans != null) {
            folderBeans.stream().map((bean) -> new TreeItem<>(bean)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/folder.png").toExternalForm()));
                return item;
            }).forEachOrdered((item) -> {
                folderTreeView.getRoot().getChildren().add(item);
            });
        }

        // Open the tree
        folderTreeView.getRoot().setExpanded(true);
    }
    /**
     * Given cell label, retrieves the folder name when a folder is clicked
     * or dragged on. 
     * @param label
     * @return  folder name
     */
    private String getFolderNameFromString(String label) {
        String folderName = "NONE";
        if (label.startsWith("T")) {
            int startPosition = label.indexOf('"') + 1;
            folderName = label.substring(startPosition, label.indexOf('"', startPosition));
        } else {
            int startPosition = label.indexOf("'") + 1;
            folderName = label.substring(startPosition, label.indexOf("'", startPosition));
        }
        return folderName;
    }
}
