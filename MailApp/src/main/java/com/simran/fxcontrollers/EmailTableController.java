package com.simran.fxcontrollers;

import com.simran.beans.EmailDataBean;
import com.simran.fxbeans.EmailTableFXBean;
import com.simran.persistence.EmailDAO;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This controller is used for the email table GUI. It supports dragging out of
 * the table into a folder tree and presentation methods that will display the
 * data in a friendly manner.
 *
 * @author Simranjot Kaur Khera
 */
public class EmailTableController {

    private final static Logger LOG = LoggerFactory.getLogger(EmailTableController.class);

    private EmailDAO emailDAO;
    
    private HtmlEditorController htmlController;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="emailTableBorderPane"
    private BorderPane emailTableBorderPane; // Value injected by FXMLLoader

    @FXML // fx:id="emailTableView"
    private TableView<EmailTableFXBean> emailTableView; // Value injected by FXMLLoader

    @FXML // fx:id="fromColumn"
    private TableColumn<EmailTableFXBean, String> fromColumn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectColumn"
    private TableColumn<EmailTableFXBean, String> subjectColumn; // Value injected by FXMLLoader

    @FXML // fx:id="dateColumn"
    private TableColumn<EmailTableFXBean, String> dateColumn; // Value injected by FXMLLoader

    /**
     * Called when an email is being dragged from the table. It stores
     * the email's ID on the DragBoard for the tree to have access to. 
     * The tree needs access to the emailID so it can change its location and update the database. 
     * This is also used to give the emailID to the Html editor when displaying the contents of an email. 
     * @param event
     * @param row 
     */
    void emailRowDragDetected(MouseEvent event, TableRow row) {

        Dragboard db = row.startDragAndDrop(TransferMode.ANY);
        EmailTableFXBean emailDraggedBean = (EmailTableFXBean)row.getItem();
        
        LOG.info("Email being dragged ID "+ emailDraggedBean.getEmailID());
        
        ClipboardContent content = new ClipboardContent();
        content.putString(emailDraggedBean.getEmailID()+"");
        
        db.setContent(content);
        event.consume();
    }

    /**
     * Called when the email has been dragged into folder tree.
     *
     * @param event
     */
    @FXML
    void emailTableDragDone(DragEvent event) {
        LOG.info("emailtableDragDone");
        //TODO update database with data in dragboard
    }

   /* public void onEmailClicked(MouseEvent event, TableRow<EmailTableFXBean> row ){
        LOG.info("Email clicked");
        row.sour
        this.htmlController.showEmailOnEditor();
        
    }*/
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert emailTableBorderPane != null : "fx:id=\"emailTableBorderPane\" was not injected: check your FXML file 'EmailTable.fxml'.";
        assert emailTableView != null : "fx:id=\"emailTableView\" was not injected: check your FXML file 'EmailTable.fxml'.";
        assert fromColumn != null : "fx:id=\"fromColumn\" was not injected: check your FXML file 'EmailTable.fxml'.";
        assert subjectColumn != null : "fx:id=\"subjectColumn\" was not injected: check your FXML file 'EmailTable.fxml'.";
        assert dateColumn != null : "fx:id=\"dateColumn\" was not injected: check your FXML file 'EmailTable.fxml'.";

        LOG.info("Initialize");
        //Setting the column with the appropriate property in the bean
        fromColumn.setCellValueFactory(cellData -> cellData.getValue().fromProperty());
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue().subjectProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().dateProperty());

        this.emailTableView.getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> this.htmlController.showEmailOnEditor(newValue));
        
                //Setting row event to be able to drag a row from the email table.
        emailTableView.setRowFactory(tableView -> {
            TableRow<EmailTableFXBean> row = new TableRow<>();
            
            row.setOnDragDetected(event -> {
                emailRowDragDetected(event, row);
            });
            
            return row;
        });
        adjustColumnWidths();
    }


    /**
     * The RootLayoutController calls this method to provide a reference to the
     * EmailDAO object which will be used to make changes to the database as the
     * user drags and drops emails.
     * Only 1 emailDAO object is being passed around by the root controller. 
     *
     * @param emailDataBean
     * @throws SQLException
     */
    public void setEmailDAO(EmailDAO emailDataBean) throws SQLException {
        this.emailDAO = emailDataBean;
    }
    
    public void setHtmlController(HtmlEditorController htmlController){
        this.htmlController = htmlController;
    }

    /**
     * The table displays the emails data in the given folder. 
     * @throws SQLException
     */
    public void displayTheTable(String folderName) throws SQLException {
        LOG.info("display email table data");
        
        // Getting emails in folder in tableBean form 
        List<EmailDataBean> emailDataBeans = this.emailDAO.findEmailsInFolder(folderName);
        ObservableList<EmailTableFXBean> emailTableBeans = convertToEmailTableFXBean(emailDataBeans);
       

        
        //Adding new row to table
        emailTableView.setItems(emailTableBeans);
    }

    /**
     * The FolderTreeController needs a reference to the this controller. With
     * that reference it can call this method to retrieve a reference to the
     * TableView and change its content
     *
     * @return ref to table
     */
    public TableView<EmailTableFXBean> getEmailDataTable() {
        return this.emailTableView;
    }
    
    /**
     * Adjusts column width to fit table content
     */
    private void adjustColumnWidths() {
        LOG.info("adjusting widths");
        double width = emailTableBorderPane.getPrefWidth();
        fromColumn.setPrefWidth(width * 0.35);
        subjectColumn.setPrefWidth(width * 0.35);
        dateColumn.setPrefWidth(width * 0.35);
    }
        
    /**
     * Uses convertToEmailTableFXBean overload method to convert a list of emailDataBeans to 
     * an observable list of email table beans. These beans can then be used to display data on the email table. 
     * @param emailDataBeans
     * @return Observable list of emailTable beans. 
     * @see displayTheTable
     */
    private ObservableList<EmailTableFXBean> convertToEmailTableFXBean(List<EmailDataBean> emailDataBeans){
        List<EmailTableFXBean> emailTableBeans = new ArrayList<EmailTableFXBean>();
        
        for (EmailDataBean emailDataBean : emailDataBeans){
            emailTableBeans.add(convertToEmailTableFXBean(emailDataBean));
        }
        ObservableList<EmailTableFXBean> observableTableBeans = FXCollections.observableList(emailTableBeans);
        return observableTableBeans;
    }
    
    /**
     * Converts a single EmailDataBean to an EmailTableDataFXBean. 
     * Information needed for the table are extracted from the emailDataBean and put into the 
     * table bean. Table bean is then used to display the emails on the table. 
     * @param emailDataBean
     * @return  Table beans necessary to show data to user. 
     */
    private EmailTableFXBean convertToEmailTableFXBean(EmailDataBean emailDataBean){
        EmailTableFXBean emailTableBean = new EmailTableFXBean();
        
        if (emailDataBean.getReceivedDate() != null ){
             emailTableBean.setDate(emailDataBean.getReceivedDate().toString());
        }
        emailTableBean.setEmailID(emailDataBean.getId());
        emailTableBean.setSubject(emailDataBean.email.subject());
        emailTableBean.setFrom(emailDataBean.email.from().toString());
        LOG.info("Transformed to table data: "+ emailTableBean.toString());
        return emailTableBean;
    }

}
