package com.simran.fxcontrollers;

import com.simran.data.FXBeansData;
import com.simran.mailapp.MainAppFX;
import com.simran.persistence.EmailDAO;
import com.simran.persistence.EmailDAOImpl;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This brings together all the other controllers and displays the final version
 * of the gui
 *
 * @author Simranjot Kaur Khera
 */
public class RootLayoutController {

    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutController.class);

    private EmailDAO emailDAO = new EmailDAOImpl();

    private EmailTableController emailTableController;

    private FolderTreeController folderTreeController;

    private HtmlEditorController htmlEditorController;

    private MainAppFX mainAppFX;

    private PropertiesFormController propertiesFormController;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="treeSplit"
    private BorderPane treeSplit; // Value injected by FXMLLoader

    @FXML // fx:id="emailTableSplit"
    private BorderPane emailTableSplit; // Value injected by FXMLLoader

    @FXML // fx:id="htmlEditorSplit"
    private BorderPane htmlEditorSplit; // Value injected by FXMLLoader

    @FXML
    void onAbout(ActionEvent event) {
        //TODO: show alert with information
    }

    /**
     * Shows file chooser to get chosen attachment.
     * @param event 
     */
    @FXML
    void onAddAttachment(ActionEvent event) {
        LOG.info("Choosing attachment - show ");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File attachment = fileChooser.showOpenDialog(this.mainAppFX.getStage());
        this.htmlEditorController.setAttachment(attachment);
    }

    @FXML
    void onClose(ActionEvent event) {
        Platform.exit();
    }

    /**
     * On save attachments can is called when a user tries to save an attachment to their device. 
     * This uses FileChooser to store the image to the device. The image to be stores on device is 
     * obtained from htmleditor controller.
     * @param event 
     */
    @FXML
    void onSaveAttachments(ActionEvent event) {
        List<File> attachments = this.htmlEditorController.getFoundAttachments();
        
        if(attachments.size() > 0){
            FileChooser fileChooser= new FileChooser();
            fileChooser.setTitle("Save Image");
            File file = fileChooser.showSaveDialog(this.mainAppFX.getStage());
            if (file != null) {
                
                Image image = new Image(attachments.get(0).toURI().toString()); 
                try {
                    ImageIO.write(SwingFXUtils.fromFXImage(image,
                        null), "png", file);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    @FXML
    void onNewEmailMenuClicked(ActionEvent event) {
        LOG.info("New email clicked");
        this.htmlEditorController.clearEditor();
    }

    /**
     * Under File menu item, if config is clicked, it will show the
     * configuration table to make changes to it
     *
     * @param event
     */
    @FXML
    void onConfigurationClicked(ActionEvent event) {
        LOG.info("ConfigClicked");
        this.mainAppFX.initMailConfigLayout();
    }

    /**
     * Enables us to move from one layout to another.
     *
     * @param mainAppFX
     */
    public void setMain(MainAppFX mainAppFX) {
        this.mainAppFX = mainAppFX;
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        LOG.info("initialize");
        assert treeSplit != null : "fx:id=\"treeSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert emailTableSplit != null : "fx:id=\"emailTableSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";
        assert htmlEditorSplit != null : "fx:id=\"htmlEditorSplit\" was not injected: check your FXML file 'RootLayout.fxml'.";

        //set all the split positions with their content
        treeViewLayout();
        emailTableLayout();
        htmlEditorLayout();
        this.htmlEditorController.setEmailTableController(emailTableController);
        this.htmlEditorController.setRootController(this);
        this.emailTableController.setHtmlController(htmlEditorController);
        this.folderTreeController.setRootLayout(this);
        setTableControllerToTree();

        try {

            folderTreeController.displayTree();
            emailTableController.displayTheTable("INBOX");
        } catch (SQLException ex) {
            LOG.error("error loading data", ex);
            errorAlert("initilize()");
            Platform.exit();
        }

    }

    /**
     * Send the reference to the emailTableController to the
     * folderTreeController
     */
    private void setTableControllerToTree() {
        folderTreeController.setTableController(emailTableController);
    }

    /**
     * The TreeView Layout. Loads the controller for the FolderTree and adds its
     * root to the split position
     */
    private void treeViewLayout() {
        LOG.info("treeViewLayout - setting layout");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutController.class
                    .getResource("/fxml/FolderTree.fxml"));

            BorderPane treeView = (BorderPane) loader.load();

            // Give the treeview controller reference to the emailDAO object.
            folderTreeController = loader.getController();
            folderTreeController.setEmailDAO(emailDAO);

            //Adding tree to the GUI
            treeSplit.getChildren().add(treeView);
        } catch (IOException ex) {
            LOG.error("treeview error", ex);
            errorAlert("treeViewLayout()");
            Platform.exit();
        }
    }

    /**
     * The TableView Layout. Loads the email table controller and sets its root
     * to the split position.
     */
    private void emailTableLayout() {
        LOG.info("emailTableLayout - setting layout");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutController.class
                    .getResource("/fxml/EmailTable.fxml"));
            BorderPane tableView = (BorderPane) loader.load();

            // Give the controller the data object.
            emailTableController = loader.getController();
            emailTableController.setEmailDAO(emailDAO);

            emailTableSplit.getChildren().add(tableView);
        } catch (SQLException | IOException ex) {
            LOG.error("emailTableLayout error", ex);
            errorAlert("emailTableLayout()");
            Platform.exit();
        }
    }

    /**
     * The HTMLEditor Layout. Loads the html editor controller and sets its root
     * to the split position.
     */
    private void htmlEditorLayout() {
        LOG.info("htmlEditorLayout - setting layout");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutController.class
                    .getResource("/fxml/HtmlEditor.fxml"));
            BorderPane htmlView = (BorderPane) loader.load();

            // Give the controller the data object.
            htmlEditorController = loader.getController();
            htmlEditorController.setEmailDAO(emailDAO);

            //giving position on the root layout.
            htmlEditorSplit.setCenter(htmlView);

        } catch (IOException ex) {
            LOG.error("htmlEditorLayout error", ex);
            errorAlert("htmlEditorLayout()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param message to be printed
     */
    public void errorAlert(String msg) {
        LOG.info("Showing error message");
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Mail Error");
        dialog.setHeaderText("Email Error");
        dialog.setContentText(msg);
        dialog.show();
    }

}
