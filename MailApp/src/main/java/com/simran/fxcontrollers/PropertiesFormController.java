package com.simran.fxcontrollers;

import com.simran.fxbeans.MailConfigBean;
import com.simran.business.PropertiesManager;
import com.simran.mailapp.MainAppFX;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

public class PropertiesFormController {

    private MailConfigBean mailConfigBean;

    private PropertiesManager propertiesManager;

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);

    private Stage primaryStage;
    
    private MainAppFX mainAppFX; 

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="userNameField"
    private TextField userNameField; // Value injected by FXMLLoader

    @FXML // fx:id="emailAddressField"
    private TextField emailAddressField; // Value injected by FXMLLoader

    @FXML // fx:id="mailPasswordField"
    private TextField mailPasswordField; // Value injected by FXMLLoader

    @FXML // fx:id="imapUrlField"
    private TextField imapUrlField; // Value injected by FXMLLoader

    @FXML // fx:id="smtpUrlField"
    private TextField smtpUrlField; // Value injected by FXMLLoader

    @FXML // fx:id="imapPortField"
    private TextField imapPortField; // Value injected by FXMLLoader

    @FXML // fx:id="smtpPortField"
    private TextField smtpPortField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlUrlField"
    private TextField mysqlUrlField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlDatabaseField"
    private TextField mysqlDatabaseField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPortField"
    private TextField mysqlPortField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlUserField"
    private TextField mysqlUserField; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlPasswordField"
    private TextField mysqlPasswordField; // Value injected by FXMLLoader


    /**
     * When user cancels, the program exits
     *
     * @param event
     */
    @FXML
    void onPressCancel(ActionEvent event) {
        this.mainAppFX.initRootLayout();
    }

    /**
     * Enables us to move from one layout to another.
     *
     * @param mainAppFX
     */
    public void setMain(MainAppFX mainAppFX) {
        this.mainAppFX = mainAppFX;
    }

    /**
     * Saves all the data from the form and write it to the MailConfig
     * properties file. After saving the data, we display the mail root layout.
     *
     * @param event
     * @throws IOException
     * @throws Exception
     */
    @FXML
    void onPressSave(ActionEvent event) throws IOException, Exception {
        propertiesManager.writeTextProperties("", "MailConfig", mailConfigBean);
        //After saving the bean, we show the rootlayout 
        mainAppFX.start(this.primaryStage);
    }

    /**
     * Get a reference to the stage. This will help move from the mailconfig
     * form stage to the root layout.
     *
     * @param primaryStage
     * @see onPressSave()
     */
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    /**
     * Loads the data from MailConfigPropertie file. If there is no data, it
     * will display the form with empty text fields.
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        LOG.info("Initialize");
        assert userNameField != null : "fx:id=\"userNameField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert emailAddressField != null : "fx:id=\"emailAddressField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mailPasswordField != null : "fx:id=\"mailPasswordField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert imapUrlField != null : "fx:id=\"imapUrlField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert smtpUrlField != null : "fx:id=\"smtpUrlField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert imapPortField != null : "fx:id=\"imapPortField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert smtpPortField != null : "fx:id=\"smtpPortField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlUrlField != null : "fx:id=\"mysqlUrlField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlDatabaseField != null : "fx:id=\"mysqlDatabaseField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlPortField != null : "fx:id=\"mysqlPortField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlUserField != null : "fx:id=\"mysqlUserField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";
        assert mysqlPasswordField != null : "fx:id=\"mysqlPasswordField\" was not injected: check your FXML file 'PropertiesForm.fxml'.";

        mailConfigBean = new MailConfigBean();
        propertiesManager = new PropertiesManager();

        try {
            propertiesManager.loadTextProperties(mailConfigBean, "", "MailConfig");
            LOG.info("Done loading bean");
        } catch (IOException ex) {
            //TODO SHOW DIALOG ABOUT FILE FAILURE
            LOG.info("Error: file failure");
        }
    }

    /**
     * Does initialization of bean to be filled and resources manager to be
     * filled with. This bean must be initialized before being doing binding.
     *
     * @param propertiesManager
     * @param mailConfigBean
     */
    public void setupProperties(PropertiesManager propertiesManager, MailConfigBean mailConfigBean) {
        this.propertiesManager = propertiesManager;
        this.mailConfigBean = mailConfigBean;
        doBindings();
    }

    /**
     * Binds all fields of the guy to the MailConfigBean fx bean. This enables
     * all the changes of made in the GUI reflect in the bean and vice versa.
     */
    private void doBindings() {
        LOG.debug("PropertyBean :" + (mailConfigBean == null));
        Bindings.bindBidirectional(userNameField.textProperty(), mailConfigBean.userNameProperty());
        Bindings.bindBidirectional(emailAddressField.textProperty(), mailConfigBean.emailAddressProperty());
        Bindings.bindBidirectional(mailPasswordField.textProperty(), mailConfigBean.mailPasswordProperty());
        Bindings.bindBidirectional(imapUrlField.textProperty(), mailConfigBean.imapURLProperty());
        Bindings.bindBidirectional(smtpUrlField.textProperty(), mailConfigBean.smtpURLProperty());
        Bindings.bindBidirectional(imapPortField.textProperty(), mailConfigBean.imapPortProperty());
        Bindings.bindBidirectional(smtpPortField.textProperty(), mailConfigBean.smtpPortProperty());
        Bindings.bindBidirectional(mysqlUrlField.textProperty(), mailConfigBean.mysqlURLProperty());
        Bindings.bindBidirectional(mysqlDatabaseField.textProperty(), mailConfigBean.mysqlDatabaseProperty());
        Bindings.bindBidirectional(mysqlPortField.textProperty(), mailConfigBean.mysqlPortProperty());
        Bindings.bindBidirectional(mysqlUserField.textProperty(), mailConfigBean.mysqlUserProperty());
        Bindings.bindBidirectional(mysqlPasswordField.textProperty(), mailConfigBean.mysqlPasswordProperty());

    }
}
