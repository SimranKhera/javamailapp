package com.simran.exceptions;

/**
 * This error is used for when the CC email addresses are invalid
 * @author Simranjot Kaur Khera
 */
public class InvalidCCAddressException extends Exception {

    public InvalidCCAddressException(String errorMessage) {
        super(errorMessage);
    }
}
