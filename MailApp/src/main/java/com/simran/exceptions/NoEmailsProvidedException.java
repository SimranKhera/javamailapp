/*
 *When no emails are provided, this exception must be thrown
 */
package com.simran.exceptions;

/**
 *
 * @author Simranjot Kaur Khera
 */
public class NoEmailsProvidedException extends Exception{
   
    public NoEmailsProvidedException(String errorMessage) {
        super(errorMessage);
    }
}
