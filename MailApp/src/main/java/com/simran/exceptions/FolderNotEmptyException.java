
package com.simran.exceptions;

/**
 * This is error is thrown when a folder with emails inside 
 * is about to be deleted. 
 * @author Simranjot Kaur Khera
 */
public class FolderNotEmptyException extends Exception{
    
    public FolderNotEmptyException(String errorMessage){
        super(errorMessage);
    }
}
