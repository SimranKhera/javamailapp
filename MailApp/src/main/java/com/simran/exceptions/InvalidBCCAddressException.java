
package com.simran.exceptions;

/**
 * This exception is used when a bcc email address is invalid
 * @author Simranjot Kaur Khera
 */
public class InvalidBCCAddressException extends Exception{
    
    public InvalidBCCAddressException(String errorMessage){
        super(errorMessage);
    }
}
