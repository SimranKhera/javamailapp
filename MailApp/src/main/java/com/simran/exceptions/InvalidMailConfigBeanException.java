
package com.simran.exceptions;

/**
 * This exception is used when mailconfigbean is null
 * @author Simranjot Kaur Khera
 */
public class InvalidMailConfigBeanException extends Exception {
    
    public InvalidMailConfigBeanException(String errorMessage){
        super(errorMessage);
    }
}
