
package com.simran.exceptions;

/**
 * This error occurs in a case that an email is being moved to
 * a folder that does not exist in the database. 
 * It is thrown when the folderID does not exist in the database. 
 * @author Simranjot Kaur Khera
 */
public class InvalidFolderIDException extends Exception{
    
    public InvalidFolderIDException(String errorMessage){
        super(errorMessage);
    }
}
