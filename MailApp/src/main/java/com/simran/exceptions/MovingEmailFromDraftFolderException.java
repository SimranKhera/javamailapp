
package com.simran.exceptions;

/**
 * This exception is throw when user tries to move email out of draft folder. 
 * @author Simranjot Kaur Khera
 */
public class MovingEmailFromDraftFolderException extends Exception{
    
    public MovingEmailFromDraftFolderException(String errorMessage){
        super(errorMessage);
    }
}
