
package com.simran.exceptions;

/**
 *This exception is used when a to address is invalid
 * @author Simranjot Kaur Khera
 */
public class InvalidToAddressException extends Exception {

    public InvalidToAddressException(String errorMessage) {
        super(errorMessage);
    }
}
