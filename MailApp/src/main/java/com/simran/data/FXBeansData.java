
package com.simran.data;

import com.simran.fxbeans.EmailTableFXBean;
import com.simran.fxbeans.FolderTreeDataBean;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class provides fake data to be used for the gui presentation.
 * It is only used to see how the data is presented on the GUI
 * @author Simranjot Kaur Khera
 */
public class FXBeansData {
    
    /**
     * Provides fake data for the email table.
     * @return 
     */
    public ObservableList<EmailTableFXBean> getEmailTableData(){
        
        ArrayList<EmailTableFXBean> emailTableData= new ArrayList<EmailTableFXBean>();
        
        emailTableData.add(new EmailTableFXBean("fromsomeone","some subject1","2020-10-20",1));
        emailTableData.add(new EmailTableFXBean("fromsomeon","some subject2","2020-11-20",2));
        emailTableData.add(new EmailTableFXBean("fromsomeo","some subject3","2020-12-20",3));
        emailTableData.add(new EmailTableFXBean("fromsome","some subject4","2020-09-20",4));
        
        return FXCollections.observableArrayList(emailTableData);
    }
    
    /**
     * Provides fake data for the folder tree controller
     * @return 
     */
    public ObservableList<FolderTreeDataBean> getFolderData(){
        ArrayList<FolderTreeDataBean> folderData= new ArrayList<FolderTreeDataBean>();
        
        folderData.add(new FolderTreeDataBean("Folder 1",0));
        folderData.add(new FolderTreeDataBean("Folder 2",1));
        folderData.add(new FolderTreeDataBean("Folder 3",2));
        
        return FXCollections.observableArrayList(folderData);
        
    }
    
}
