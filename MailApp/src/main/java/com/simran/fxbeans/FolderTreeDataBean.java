
package com.simran.fxbeans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Contains data to be presented for the folder tree in the GUI.
 * @author Simranjot Kaur Khera
 */
public class FolderTreeDataBean {
    private StringProperty name;
    private IntegerProperty folderID;

    public FolderTreeDataBean(String name, int folderID){
        this.name= new SimpleStringProperty(name);
        this.folderID= new SimpleIntegerProperty(folderID);
    }
    
    /**
     * Default constructor
     */
    public FolderTreeDataBean(){
        this.name= new SimpleStringProperty("");
        this.folderID= new SimpleIntegerProperty(-1);
    }
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty(){
        return name;
    }
    public int getID() {
        return folderID.get();
    }

    public void setID(int ID) {
        this.folderID.set(ID);
    }
    
    public IntegerProperty idProperty(){
        return this.folderID;
    }
    
}
