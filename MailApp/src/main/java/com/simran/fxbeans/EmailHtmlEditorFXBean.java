
package com.simran.fxbeans;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * This class holds data of the htmlEditor.
 * It consists of data to be taken from the user when creating and
 * sending or saving an email
 * @author Simranjot Kaur Khera
 */
public class EmailHtmlEditorFXBean {
    
    private StringProperty to;
    private StringProperty cc;
    private StringProperty bcc;
    private StringProperty subject;
    private StringProperty htmlMessage;

    public EmailHtmlEditorFXBean(String to, String cc, String bcc, String subject, String htmlMessage) {
        this.to = new SimpleStringProperty(to);
        this.cc = new SimpleStringProperty(cc);
        this.bcc = new SimpleStringProperty(bcc);
        this.subject = new SimpleStringProperty(subject);
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
    }

    /**
     * Default constructor
     */
    public EmailHtmlEditorFXBean(){
        this.bcc= new SimpleStringProperty("");
        this.cc= new SimpleStringProperty("");
        this.to= new SimpleStringProperty("");
        this.subject= new SimpleStringProperty("");
        this.htmlMessage= new SimpleStringProperty("");
    }
    public String getBcc() {
        return bcc.get();
    }

    public void setBcc(String bcc) {
        this.bcc.set(bcc);
    }
    
    public StringProperty bccProperty(){
        return this.bcc;
    }

    public String getTo() {
        return to.get();
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public StringProperty toProperty(){
        return this.to;
    }
    
    public String getCc() {
        return cc.get();
    }

    public void setCc(String cc) {
        this.cc.set(cc);
    }
    
    public StringProperty ccProperty(){
        return this.cc;
    }
    
    public String getSubject() {
        return subject.get();
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }
    
    public StringProperty subjectProperty(){
        return this.subject;
    }

    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }
    
    public StringProperty htmlProperty(){
        return this.htmlMessage;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmailDataFXBean{to=").append(to.get());
        sb.append(", cc=").append(cc.get());
        sb.append(", bcc=").append(bcc.get());
        sb.append(", subject=").append(subject.get());
        sb.append(", htmlMessage=").append(htmlMessage.get());
        sb.append('}');
        return sb.toString();
    }
    
}
