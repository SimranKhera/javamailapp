
package com.simran.fxbeans;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class contains data to be presented on the email table in the GUI.
 * @author Simranjot Kaur Khera
 */
public class EmailTableFXBean {
    private StringProperty from;
    private StringProperty subject;
    private StringProperty date;
    private IntegerProperty emailID;

    public EmailTableFXBean(String from, String subject, String date, int emailID) {
        this.from= new SimpleStringProperty(from);
        this.subject= new SimpleStringProperty(subject);
        this.date= new SimpleStringProperty(date);
        this.emailID= new SimpleIntegerProperty(emailID);
    }

    /**
     * Default constructor
     */
    public EmailTableFXBean(){
        this.from= new SimpleStringProperty("");
        this.subject=new SimpleStringProperty("");
        this.date=new SimpleStringProperty("");
        this.emailID= new SimpleIntegerProperty(-1);
    }
    
    public int getEmailID() {
        return emailID.get();
    }

    public void setEmailID(int emailID) {
        this.emailID.set(emailID);
    }
   
    
    public String getFrom() {
        return from.get();
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public StringProperty fromProperty(){
        return this.from;
    }
    public String getSubject() {
        return subject.get();
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public StringProperty subjectProperty(){
        return this.subject;
    }
    public String getDate() {
        return date.get();
    }

    public void setDate(String date) {
        this.date.set(date);
    }

    public StringProperty dateProperty(){
        return this.date;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmailTableDataFXBean{from=").append(from.get());
        sb.append(", subject=").append(subject.get());
        sb.append(", date=").append(date.get());
        sb.append(", emailID=").append(emailID.get());
        sb.append('}');
        return sb.toString();
    }
    
}
