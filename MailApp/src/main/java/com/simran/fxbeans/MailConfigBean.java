package com.simran.fxbeans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This represents a javafx bean that will hold the users credentials 
 * as well information about the database to be used to store the emails.
 * @author This code has been taken from example file given by Ken Fogel
 */
public class MailConfigBean {

    private StringProperty userName;
    private StringProperty emailAddress;
    private StringProperty mailPassword;
    private StringProperty imapURL;
    private StringProperty smtpURL;
    private StringProperty imapPort;
    private StringProperty smtpPort;
    private StringProperty mysqlURL;
    private StringProperty mysqlDatabase;
    private StringProperty mysqlPort;
    private StringProperty mysqlUser;
    private StringProperty mysqlPassword;

    public MailConfigBean(String userName, String emailAddress, String mailPassword,
            String imapURL, String smtpURL, String imapPort,
            String smtpPort, String mysqlURL, String mysqlDatabase,
            String mysqlPort, String mysqlUser, String mysqlPassword) {
        this.userName = new SimpleStringProperty(userName);
        this.emailAddress = new SimpleStringProperty(emailAddress);
        this.mailPassword = new SimpleStringProperty(mailPassword);
        this.imapURL = new SimpleStringProperty(imapURL);
        this.smtpURL = new SimpleStringProperty(smtpURL);
        this.imapPort = new SimpleStringProperty(imapPort);
        this.smtpPort = new SimpleStringProperty(smtpPort);
        this.mysqlURL = new SimpleStringProperty(mysqlURL);
        this.mysqlDatabase = new SimpleStringProperty(mysqlDatabase);
        this.mysqlPort = new SimpleStringProperty(mysqlPort);
        this.mysqlUser = new SimpleStringProperty(mysqlUser);
        this.mysqlPassword = new SimpleStringProperty(mysqlPassword);
    }

    public MailConfigBean() {
        this("", "", "", "", "", "", "", "", "", "", "", "");
    }

    public String getUserName() {
        return userName.get();
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public String getEmailAddress() {
        return emailAddress.get();
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress.set(emailAddress);
    }

    public StringProperty emailAddressProperty() {
        return emailAddress;
    }

    public String getMailPassword() {
        return mailPassword.get();
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword.set(mailPassword);
    }

    public StringProperty mailPasswordProperty() {
        return mailPassword;
    }

    public String getImapURL() {
        return imapURL.get();
    }

    public void setImapURL(String imapURL) {
        this.imapURL.set(imapURL);
    }

    public StringProperty imapURLProperty() {
        return imapURL;
    }

    public String getSmtpURL() {
        return smtpURL.get();
    }

    public void setSmtpURL(String smtpURL) {
        this.smtpURL.set(smtpURL);
    }

    public StringProperty smtpURLProperty() {
        return smtpURL;
    }

    public String getImapPort() {
        return imapPort.get();
    }

    public void setImapPort(String imapPort) {
        this.imapPort.set(imapPort);
    }

    public StringProperty imapPortProperty() {
        return imapPort;
    }

    public String getSmtpPort() {
        return smtpPort.get();
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort.set(smtpPort);
    }

    public StringProperty smtpPortProperty() {
        return smtpPort;
    }

    public String getMysqlURL() {
        return mysqlURL.get();
    }

    public void setMysqlURL(String mysqlURL) {
        this.mysqlURL.set(mysqlURL);
    }

    public StringProperty mysqlURLProperty() {
        return mysqlURL;
    }

    public String getMysqlDatabase() {
        return mysqlDatabase.get();
    }

    public void setMysqlDatabase(String mysqlDatabase) {
        this.mysqlDatabase.set(mysqlDatabase);
    }

    public StringProperty mysqlDatabaseProperty() {
        return mysqlDatabase;
    }

    public String getMysqlPort() {
        return mysqlPort.get();
    }

    public void setMysqlPort(String mysqlPort) {
        this.mysqlPort.set(mysqlPort);
    }

    public StringProperty mysqlPortProperty() {
        return mysqlPort;
    }

    public String getMysqlUser() {
        return mysqlUser.get();
    }

    public void setMysqlUser(String mysqlUser) {
        this.mysqlUser.set(mysqlUser);
    }

    public StringProperty mysqlUserProperty() {
        return mysqlUser;
    }

    public String getMysqlPassword() {
        return mysqlPassword.get();
    }

    public void setMysqlPassword(String mysqlPassword) {
        this.mysqlPassword.set(mysqlPassword);
    }

    public StringProperty mysqlPasswordProperty() {
        return mysqlPassword;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PropertyBean{userName=").append(userName);
        sb.append(", emailAddress=").append(emailAddress);
        sb.append(", mailPassword=").append(mailPassword);
        sb.append(", imapURL=").append(imapURL);
        sb.append(", smtpURL=").append(smtpURL);
        sb.append(", imapPort=").append(imapPort);
        sb.append(", smtpPort=").append(smtpPort);
        sb.append(", mysqlURL=").append(mysqlURL);
        sb.append(", mysqlDatabase=").append(mysqlDatabase);
        sb.append(", mysqlPort=").append(mysqlPort);
        sb.append(", mysqlUser=").append(mysqlUser);
        sb.append(", mysqlPassword=").append(mysqlPassword);
        sb.append('}');
        return sb.toString();
    }
}
