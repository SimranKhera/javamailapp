package com.simran.beans;

import java.time.LocalDateTime;
import jodd.mail.Email;
import jodd.mail.EmailAddress;

/**
 * Is used to hold an email with the fields of the email table
 * in the database. 
 * @author Simranjot Kaur Khera
 */
public class EmailDataBean {
    private int id;
    private int folderid;
    private LocalDateTime receivedDate;
    public Email email;

    /**
     * Non-default constructor - sets all properties
     * @param id
     * @param folderid
     * @param receivedDate
     * @param email 
     */
    public EmailDataBean(int id, int folderid, LocalDateTime receivedDate, Email email) {
        this.id = id;
        this.folderid = folderid;
        this.receivedDate = receivedDate;
        this.email = email;
    }
    /**
     * Non-default constructor
     * @param folderid
     * @param receivedDate
     * @param email 
     */
    public EmailDataBean(int folderid, LocalDateTime receivedDate, Email email){
        this.folderid = folderid;
        this.receivedDate = receivedDate;
        this.email = email;
    }
    /**
     * Default Constructor
     */
    public EmailDataBean(){
        this(-1,-1, null, new Email());
    }
    
    public int getId() {
        return id;
    }

    public int getFolderid() {
        return folderid;
    }

    public LocalDateTime getReceivedDate() {
        return receivedDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFolderid(int folderid) {
        this.folderid = folderid;
    }

    public void setReceivedDate(LocalDateTime receivedDate) {
        this.receivedDate = receivedDate;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmailDataBean{id=").append(id);
        sb.append(", folderid=").append(folderid);
        sb.append(", receivedDate= ").append(receivedDate);
        sb.append(", sentDate= ").append(email.sentDate());
        sb.append(", subject= ").append(email.subject());
        sb.append(", email FROM= ").append(email.from());
        sb.append(", TO = ");
        for(EmailAddress address : email.to()){
            sb.append(address);
        }
        sb.append(", CC = ");
        for(EmailAddress address : email.cc()){
            sb.append(address);
        }
        
        sb.append(", BCC = ");
        for(EmailAddress address : email.bcc()){
            sb.append(address);
        }
        sb.append('}');
        return sb.toString();
    }

    
    
    
}
