package com.simran.business;

import com.simran.exceptions.*;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.mail.Flags;
import jodd.mail.EmailFilter;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import com.simran.fxbeans.MailConfigBean;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to send and receive emails. It can send/receive emails
 * containing attachments,textMessages, htmlMessages, or empty messages with
 * subject. This class uses the MailConfigBean to get the credentials of the
 * current user (password, email) and information to open server sessions.
 *
 * @author Simranjot Kaur Khera
 */
public class SendAndReceive {

    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceive.class);

    /**
     * The send method calls createEmail helper method to create Email object.
     * It then creates the SMPT server session, sends the email and returns the
     * email. Returns an exception if email could not be sent during session.
     * If no address is provided, it throws an error, otherwise it checks for invalid addresses.
     * @param toAddresses
     * @param ccAddresses
     * @param bccAddresses
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param regAttachments
     * @param embAttachments
     * @return Email
     */
    public Email sendEmail(List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String subject,
            String textMessage, String htmlMessage, List<File> regAttachments, List<File> embAttachments, MailConfigBean mailConfigBean)
            throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {

        Email email = null;
        //create email with all data provided, checks for mailconfig validity
        //throws error if credentials are not valid or if there are no emails to send to
        if (checkMailConfigBean(mailConfigBean) && checkForNoEmailAddresses(toAddresses,ccAddresses,bccAddresses)) {
            email = createEmail(toAddresses, ccAddresses, bccAddresses, subject, textMessage, htmlMessage,
                    regAttachments, embAttachments, mailConfigBean);
        }
        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(mailConfigBean.getSmtpURL())
                .auth(mailConfigBean.getEmailAddress(), mailConfigBean.getMailPassword())
                //.debugMode(true)
                .buildSmtpMailServer();
        
        // A session is the object responsible for communicating with the server
        // Similar to how we work with a file, we open the session, sendEmail the 
        // message and close the session. try-with-resources ensures we never 
        // forget to close the session.
        try ( SendMailSession session = smtpServer.createSession()) {
            session.open();
            session.sendMail(email);
        } catch (Exception ex) {
            LOG.error("Failure in session, could not send email ", ex);
        }
        
        LOG.info("Email sent");
        return email;
    }

    /**
     * The receiveEmail method: Creates ReceivedEmail array, checks if current
     * users email is valid, creates IMAP server session, populates the
     * ReceivedEmail array with retrieved emails from server, flags those
     * retrieved emails as SEEN, returns the retrieved emails (ReceivedEmail[])
     * Throws error if failure during session and if the current users email is
     * invalid.
     *
     * @return ReceivedEmail[] new unread emails
     */
    public ReceivedEmail[] receiveEmail(MailConfigBean mailConfigBean) throws InvalidMailConfigBeanException {

        //emails to be returned
        ReceivedEmail[] receivedEmails = new ReceivedEmail[0];
        //Check for credential validity
        if (checkMailConfigBean(mailConfigBean)) {
            
            // Create am IMAP server object
            ImapServer imapServer = MailServer.create()
                    .host(mailConfigBean.getImapURL())
                    .ssl(true)
                    .auth(mailConfigBean.getEmailAddress(), mailConfigBean.getMailPassword())
                    //.debugMode(true)
                    .buildImapMailServer();

            try ( ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                receivedEmails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
            } catch (Exception ex) {
                LOG.error("Failure in session, could not receive email ", ex);
            }

        }
        LOG.info("Emails received");
        return receivedEmails;
    }

    /**
     * createEmail is a helper method, it creates the email object to be sent
     * (used in sendEmail method). It takes all parameters as the send method
     * and calls helper methods to add them to the email object while validating
     * them. It them returns the email object.
     *
     * @param toAddresses
     * @param ccAddresses
     * @param bccAddreses
     * @param subject
     * @param textMessage
     * @param HtmlMessage
     * @param regAttachments
     * @param embAttachements
     * @return Email email to be sent
     */
    private Email createEmail(List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String subject,
            String textMessage, String htmlMessage, List<File> regAttachments, List<File> embAttachments, MailConfigBean mailConfigBean)
            throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException {
        Email email = Email.create();

        //Checking for email validity in cc,bcc and to lists
        if (addToAddressesToEmail(email, toAddresses)) {
            if (addCCAddressesToEmail(email, ccAddresses)) {
                if (addBCCAddressesToEmail(email, bccAddresses)) {
                    //setting sender email
                    email.from(mailConfigBean.getEmailAddress());
                    //Add attachments to receivedEmails
                    addEmbeddedAttachmentsToEmail(email, embAttachments);
                    addRegAttachmentsToEmail(email, regAttachments);
                    //Add strings to emails
                    email.textMessage(textMessage);
                    email.htmlMessage(htmlMessage);
                    email.subject(subject);
                }
            }
        }
        
        LOG.info("Email Created");
        //return to sendEmail method
        return email;
    }

    /**
     * The addToAddressesToEmail method: Validates all emails in the
     * toAddresses, adds them to the email object, returns true if all emails
     * are valid and have been successfully added to the email object Throws
     * error when one of the emails are invalid. This is used in the createEmail
     * method to create the email.
     *
     * @param email
     * @param toAddresses
     * @return Boolean true if successfully added to email
     */
    private boolean addToAddressesToEmail(Email email, List<String> toAddresses) throws InvalidToAddressException {
        LOG.info("Adding TO addresses");
        for (int i = 0; i < toAddresses.size(); ++i) {
            if (checkEmail(toAddresses.get(i))) {
                email.to(toAddresses.get(i));
            } else {
                LOG.error("Invalid Email: " + toAddresses.get(i));
                throw new InvalidToAddressException("Invalid email: " + toAddresses.get(i));
            }
        }
        return true;
    }

    /**
     * The addCCAddressesToEmail method: Validates all emails in the
     * ccAddresses, adds them to the email object, returns true if all emails
     * are valid and have been successfully added to the email object Throws
     * error when one of the emails are invalid. This is used in the createEmail
     * method to create the email.
     *
     * @param email
     * @param ccAddresses
     * @return Boolean true if successfully added to email
     */
    private boolean addCCAddressesToEmail(Email email, List<String> ccAddresses) throws InvalidCCAddressException {
        for (int i = 0; i < ccAddresses.size(); ++i) {
            if (checkEmail(ccAddresses.get(i))) {
                email.cc(ccAddresses.get(i));
            } else {
                LOG.error("Invalid Email: " + ccAddresses.get(i));
                throw new InvalidCCAddressException("Invalid email " + ccAddresses.get(i));
            }
        }
        return true;
    }

    /**
     * The addBCCAddressesToEmail method: Validates all emails in the
     * bccAddresses, adds them to the email object, returns true if all emails
     * are valid and have been successfully added to the email object Throws
     * error when one of the emails are invalid. This is used in the createEmail
     * method to create the email.
     *
     * @param email
     * @param bccAddresses
     * @return Boolean true if successfully added to email
     */
    private boolean addBCCAddressesToEmail(Email email, List<String> bccAddresses) throws InvalidBCCAddressException {
        for (int i = 0; i < bccAddresses.size(); ++i) {
            if (checkEmail(bccAddresses.get(i))) {
                email.bcc(bccAddresses.get(i));
            } else {
                throw new InvalidBCCAddressException("Invalid email: " + bccAddresses.get(i));
            }
        }
        return true;
    }

    /**
     * The addEmbeddedAttachmentsToEmail method adds all embedded attachments to
     * the email object. If embAttachments is empty, it is skipped.
     *
     * @param email
     * @param embAttachments
     */
    private void addEmbeddedAttachmentsToEmail(Email email, List<File> embAttachments) {
        for (int i = 0; i < embAttachments.size(); ++i) {
            email.embeddedAttachment(EmailAttachment.with().content(embAttachments.get(i)));
        }
        LOG.info("Embedded Attachments Added- addEmbeddedAttachmentsToEmail");
    }

    /**
     * The addRegAttachmentsToEmail method adds all regular attachments to the
     * email object. If attachments is empty, it is skipped.
     *
     * @param email
     * @param attachments
     */
    private void addRegAttachmentsToEmail(Email email, List<File> attachments) {
        for (int i = 0; i < attachments.size(); ++i) {
            email.attachment(EmailAttachment.with().content(attachments.get(i)));
        }
        LOG.info("Attachments Added-addRegAttachmentsToEmail ");
    }

    /**
     * Use the RFC2822AddressParser to validate that the receivedEmails string
     * could be a valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        LOG.info("Checking: " + address + " validity");
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
    /**
     * This method checks if there are no email addresses provided in any of
     * the lists. If so, it throws a custom exception, otherwise it returns true
     * @param toAddresses
     * @param ccAddresses
     * @param bccAddresses
     * @return boolean true of at least one email is provided
     * @throws NoEmailsProvidedException 
     */
    private boolean checkForNoEmailAddresses(List<String> toAddresses,List<String> ccAddresses,List<String> bccAddresses) throws NoEmailsProvidedException{
        if(toAddresses.size()!=0 || ccAddresses.size()!=0 || bccAddresses.size()!=0){
           return true; 
        }
        throw new NoEmailsProvidedException("Must provide an email");
    }
    /**
     * This method makes sure the MailConfigBean is valid.
     * throws a custom exception if any field is null
     * @param mailConfigBean
     * @return Boolean  true if valid
     * @throws InvalidMailConfigBeanException 
     */
    private boolean checkMailConfigBean(MailConfigBean mailConfigBean) throws InvalidMailConfigBeanException {
        if (mailConfigBean != null) {
            if ( mailConfigBean.getEmailAddress()!=null && checkEmail(mailConfigBean.getEmailAddress()) ) {
                if (mailConfigBean.getMailPassword() != null && mailConfigBean.getMailPassword().length() != 0) {
                    if (mailConfigBean.getImapURL() != null && mailConfigBean.getSmtpURL()!= null) {
                        return true;
                    }
                }
            }
        }
        throw new InvalidMailConfigBeanException("Invalid credentials");
    }
}
