package com.simran.business;

import com.simran.fxbeans.MailConfigBean;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages the configbean properties.
 *
 * @author This code has been taken from an example file written by Ken Fogel
 *
 */
public class PropertiesManager {

    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);

    /**
     * Updates a configBean object with the contents of the properties file
     * Checks if the file is not empty and exists before loading
     *
     * @param mailConfigBean
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final MailConfigBean mailConfigBean, final String path, final String propFileName) throws IOException {
        LOG.info("Reading MailConfig properties");
        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        //File must not be empty
        File configProperties = new File(path + propFileName + ".properties");
        if (configProperties.length() == 0) {
            return found;
        }

        // File must exist
        if (Files.exists(txtFile)) {
            try ( InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            LOG.info("Loading MailConfigProperties");
            mailConfigBean.setUserName(prop.getProperty("userName"));
            mailConfigBean.setEmailAddress(prop.getProperty("emailAddress"));
            mailConfigBean.setMailPassword(prop.getProperty("mailPassword"));
            mailConfigBean.setImapURL(prop.getProperty("imapURL"));
            mailConfigBean.setSmtpURL(prop.getProperty("smtpURL"));
            mailConfigBean.setImapPort(prop.getProperty("imapPort"));
            mailConfigBean.setSmtpPort(prop.getProperty("smtpPort"));
            mailConfigBean.setMysqlURL(prop.getProperty("mysqlURL"));
            mailConfigBean.setMysqlDatabase(prop.getProperty("mysqlDatabase"));
            mailConfigBean.setMysqlPort(prop.getProperty("mysqlPort"));
            mailConfigBean.setMysqlUser(prop.getProperty("mysqlUser"));
            mailConfigBean.setMysqlPassword(prop.getProperty("mysqlPassword"));

            found = true;
        }
        return found;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param propertyBean The bean to store into the properties
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final MailConfigBean propertyBean) throws IOException {
        LOG.info("Writing MailConfigProperties");
        Properties prop = new Properties();
        String mySQLURL= "jdbc:mysql://"+propertyBean.getMysqlURL()+":3306/EMAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        
        prop.setProperty("userName", propertyBean.getUserName());
        prop.setProperty("emailAddress", propertyBean.getEmailAddress());
        prop.setProperty("mailPassword", propertyBean.getMailPassword());
        prop.setProperty("imapURL", propertyBean.getImapURL());
        prop.setProperty("smtpURL", propertyBean.getSmtpURL());
        prop.setProperty("imapPort", propertyBean.getImapPort());
        prop.setProperty("smtpPort", propertyBean.getSmtpPort());
        prop.setProperty("mysqlURL", mySQLURL);
        prop.setProperty("mysqlDatabase", propertyBean.getMysqlDatabase());
        prop.setProperty("mysqlPort", propertyBean.getMysqlPort());
        prop.setProperty("mysqlUser", propertyBean.getMysqlUser());
        prop.setProperty("mysqlPassword", propertyBean.getMysqlPassword());

        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try ( OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "SMTP Properties");
        }

    }
}
