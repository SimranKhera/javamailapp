package com.simran.mailapp;

import com.simran.business.PropertiesManager;
import com.simran.fxbeans.MailConfigBean;
import com.simran.fxcontrollers.PropertiesFormController;
import com.simran.fxcontrollers.RootLayoutController;
import com.simran.persistence.EmailDAOImpl;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This method provides methods to show the rootlayout with all its components or
 * only the mailConfig form. 
 * @author Simranjot Kaur Khera
 */
public class MainAppFX extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);

    private Stage primaryStage;
    private Parent rootLayout;
    private Locale currentLocale;
    private MailConfigBean mailConfigBean = new MailConfigBean();
    private PropertiesManager propertiesManager = new PropertiesManager();
    private EmailDAOImpl emailDAO = new EmailDAOImpl();
    /**
     * This method sets the icon of the app and checks if the mailconfig
     * properties files is empty. If it is, it will be displayed, get the data 
     * necessary to run the mail root layout of the app.
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        LOG.info("Starting program");
        this.primaryStage = primaryStage;

        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/mail.png")));

        //Displays the mailconfig form only if the properties file is empty
        File mainConfigFile = new File("MailConfig.properties");

        //Mainconfig file must not be empty before displaying the root layout.
        if (mainConfigFile.length() == 0) {
            //sets the mailconfig form
            initMailConfigLayout();

        } else {
            //sets the rootLayout
            initRootLayout();
        }
       
    }

    /**
     * Load the layout and controller. When the RootLayoutController runs its
     * initialize method all the other containers are created.
     */
    public void initRootLayout() {

        currentLocale = Locale.getDefault();
        LOG.debug("Locale = " + currentLocale);
        this.currentLocale = new Locale("fr", "CA");
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MainAppFX.class
                    .getResource("/fxml/RootLayout.fxml"));
            this.rootLayout = (BorderPane) loader.load();
            
            this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("AppTitle"));
            RootLayoutController rootController = loader.getController();
           
            rootController.setMain(this);
            showStage();
        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("initRootLayout()");
            Platform.exit();
        }
    }

    /**
     * Sets the primaryStage title, rootLayout to the mailconfigForm and
     * displays it.
     */
    public void initMailConfigLayout() {

        currentLocale = Locale.getDefault();
        LOG.debug("Locale = " + currentLocale);
        //this.currentLocale = new Locale("fr", "CA");
        LOG.info("Setting up mailconfig layout");
        try {

            this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("title"));

            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));

            loader.setLocation(MainAppFX.class
                    .getResource("/fxml/PropertiesForm.fxml"));
            this.rootLayout = (GridPane) loader.load();

            // Retreive a refernce to the controller from the FXMLLoader and send a refference to primaryStage
            PropertiesFormController formController = loader.getController();
            // Retreive a refernce to the controller from the FXMLLoader
            PropertiesFormController controller = loader.getController();

            //provide bean the data to be saved in and the manager to be saved with 
            controller.setupProperties(propertiesManager, mailConfigBean);
            formController.setPrimaryStage(primaryStage);

            formController.setMain(this);

            showStage();
        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("initMailConfigLayout()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("sqlError"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("sqlError"));
        dialog.setContentText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString(msg));
        dialog.show();
    }
    
    /**
     * Shows currently set stage
     */
    private void showStage(){
        
        Scene scene = new Scene(this.rootLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
        LOG.info("Stage Displayed");
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
    
    public Stage getStage(){
        return this.primaryStage;
    }
}
