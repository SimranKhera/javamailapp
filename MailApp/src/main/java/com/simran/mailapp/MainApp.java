
package com.simran.mailapp;

/**
 * This is where the program starts.
 * @author Simranjot Kaur Khera
 */
public class MainApp {

    /**
     * Calls the main method in the MainApp Application
     * @param args 
     */
    public static void main(String[] args) {
        MainAppFX.main(args);
        System.exit(0);
    }
}
