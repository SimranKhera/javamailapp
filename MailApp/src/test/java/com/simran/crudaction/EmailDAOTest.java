package com.simran.crudaction;

import org.junit.Before;
import static org.junit.Assert.*;
import com.simran.beans.EmailDataBean;
import com.simran.business.SendAndReceive;
import com.simran.exceptions.InvalidBCCAddressException;
import com.simran.exceptions.InvalidCCAddressException;
import com.simran.exceptions.InvalidMailConfigBeanException;
import com.simran.exceptions.InvalidToAddressException;
import com.simran.exceptions.MovingEmailFromDraftFolderException;
import com.simran.exceptions.NoEmailsProvidedException;
import com.simran.fxbeans.FolderTreeDataBean;
import com.simran.fxbeans.MailConfigBean;
import com.simran.persistence.EmailDAOImpl;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.ReceivedEmail;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class tests all CRUD methods for the emailapp database.
 *
 * @author Simranjot Kaur Khera
 */
@Ignore
public class EmailDAOTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOTest.class);
    private final static String URL = "jdbc:mysql://localhost:3306/EMAILAPP?zeroDateTimeBehavior=CONVERT_TO_NULL";
    private final static String USER = "email";
    private final static String PASSWORD = "kfstandard";
    private SendAndReceive actionObject = new SendAndReceive();
    EmailDAOImpl emailDAO = new EmailDAOImpl();

    /**
     * Tests the createEmail method by creating an email object and trying to
     * store it in the database. The email object is acquired from
     * createEmailObject().
     *
     * @throws SQLException
     */
    @Test
    public void testCreateEmail() throws SQLException {
        LOG.info("Testing testCreateEmail");
        EmailDataBean emailData = new EmailDataBean(3, null, createEmailObject());
        int newRowKeyNumber = this.emailDAO.createEmail(emailData);
        //getting the email from database
        EmailDataBean createdEmail = this.emailDAO.findByID(newRowKeyNumber);
        assertEquals(5, createdEmail.getId());
    }

    /**
     * This method tests the occasion when we create an email that already
     * exists. It should delete the email and create it again (meant for
     * updating drafts in the program)
     *
     * @throws SQLException
     */
    @Test
    public void testCreateEmailAlreadyExists() throws SQLException {
        LOG.info("Testing testCreateEmailAlreadyExists");
        EmailDataBean emailData = new EmailDataBean(1, null, createEmailObject());
        emailData.setId(1);

        int rowKeyCreatedNumber = this.emailDAO.createEmail(emailData);
        EmailDataBean createdEmail = this.emailDAO.findByID(rowKeyCreatedNumber);

        assertEquals("Email for testing", createdEmail.email.subject());
    }

    /**
     * Tests the createFolder method by giving it a folder name. It then
     * compares the total number of records in the folder table which should be
     * 4 after adding the TestFolder.
     *
     * @throws SQLException
     */
    @Test
    public void testCreateFolder() throws SQLException {
        LOG.info("testing creating new folder");
        String folderName = "TestFolder";
        int generatedFolderKey = this.emailDAO.createFolder(folderName);
        //Added folder is the forth folder in the table. The generated key must be 4
        assertEquals(4, generatedFolderKey);
    }

    /**
     * Testing createInboxEmails by using send and receive class to send an
     * email, receive it and pass it to create method to store in DB. We then
     * retrieve the created email from DB and compare the subject with the email
     * that was sent.
     *
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException
     * @throws SQLException
     */
    @Test
    public void testCreateInboxEmails() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException, SQLException {
        LOG.info("testCreateInboxEmails");
        //Send an email to secondUser email
        this.sendEmailForTesting();

        //sleep before retrieving emails
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
        }

        MailConfigBean receiverBean = new MailConfigBean("imap.gmail.com", "seconduseracc2@gmail.com", "seconduserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "", "");
        ReceivedEmail[] receivedEmails = this.actionObject.receiveEmail(receiverBean);

        int lastGenKey = this.emailDAO.createInboxEmails(receivedEmails);
        EmailDataBean createdEmail = this.emailDAO.findByID(lastGenKey);
        assertEquals("Send Email testing DB", createdEmail.email.subject());
    }

    /**
     * Tests finding all emails from INBOX. There should be only one email with
     * "received email 1" as its subject
     *
     * @throws SQLException
     */
    @Test
    public void testFindEmailsInFolder() throws SQLException {
        LOG.info("testFindEmailsInFolder");
        List<EmailDataBean> emailBeansInFolder = this.emailDAO.findEmailsInFolder("INBOX");
        //the subject for the INBOX folder email is : "received email 1"
        assertEquals("received email 1", emailBeansInFolder.get(0).email.subject());
    }

    /**
     * Tests finding an email from database given an email id. Makes sure that
     * the emailBean returned contains the right subject.
     *
     * @throws SQLException
     */
    @Test
    public void testFindByID() throws SQLException {
        EmailDataBean retrievedBean = this.emailDAO.findByID(1);
        assertEquals("sentEmail 1", retrievedBean.email.subject());
    }

    /**
     * Tests find all folders, should return 3 folders.
     *
     * @throws SQLException
     */
    @Test
    public void testFindAllFolders() throws SQLException {
        LOG.info("testFindAllFolders");
        List<FolderTreeDataBean> folders = new ArrayList<FolderTreeDataBean>();
        folders = this.emailDAO.findAllFolders();
        assertEquals(3, folders.size());
    }

    /**
     * Tests finding folder id from given foldername: SENT
     * @throws SQLException 
     */
    @Test
    public void testFindFolderByName() throws SQLException {
        assertEquals(1, this.emailDAO.findFolderByName("SENT"));
    }
   
    /**
     * Tests updating folder of a email id. 
     * This method will then get the email by id and check if its folder id is updated. 
     * @throws SQLException
     * @throws MovingEmailFromDraftFolderException 
     */
    @Test
    public void testUpdateFolder() throws SQLException, MovingEmailFromDraftFolderException{
        this.emailDAO.updateFolder(1, "DRAFT");
        
        EmailDataBean emailChanged = this.emailDAO.findByID(1);
        //Draft folderID is 2
        assertEquals(2, emailChanged.getFolderid());
    }
    
    /**
     * Testing for MovingEmailFromDraftFolderException. 
     * The second email in db is in a draft email, this method will try to move it to a SENT email. 
     * @throws SQLException
     * @throws MovingEmailFromDraftFolderException 
     */
    @Test
    public void testUpdateFolderFromDraft() throws SQLException, MovingEmailFromDraftFolderException{
        thrown.expect(MovingEmailFromDraftFolderException.class); 
        this.emailDAO.updateFolder(2, "SENT");
    }
    
    
    /**
     * Tests deleting the first email in the table. deleteEmail() also deleted
     * the dependent table rows.
     *
     * @throws SQLException
     */
    @Test
    public void testDeleteEmail() throws SQLException {
        int numDeletedRows = this.emailDAO.deleteEmail(1);
        assertEquals(1, numDeletedRows);
    }

    
    /**
     * Creates a simple email with CC, attachment, subject and text message This
     * email object will be stored in database.
     *
     * @return
     */
    private Email createEmailObject() {
        Email email = new Email();
        email.from("firstuseracc1@gmail.com");
        email.to("fourthuseracc4@gmail.com");
        email.cc("seconduseracc2@gmail.com");
        email.subject("Email for testing");
        email.textMessage("email test text message");
        email.htmlMessage("<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img src='cid:FreeFall.jpg'>"
                + "<h2>I'm flying!</h2></body></html>");

        email.sentDate(new Date());

        //adding an attachment to email
        File waddlesImage = new File("waddles.jpg");
        email.embeddedAttachment(EmailAttachment.with().content(waddlesImage));
        return email;
    }

    /**
     * This private method helps send a single email for testing. This is used
     * to then receiveEmails and store into database. The email is always sent
     * from first user to second user.
     *
     * @return
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException
     */
    private Email sendEmailForTesting() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        MailConfigBean sendMailConfigBean = new MailConfigBean("imap.gmail.com", "firstuseracc1@gmail.com", "firstuserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "", "");

        ArrayList<String> toAddresses = new ArrayList<String>();
        toAddresses.add("seconduseracc2@gmail.com");
        return actionObject.sendEmail(toAddresses, new ArrayList<String>(), new ArrayList<String>(), "Send Email testing DB", "", "",
                new ArrayList<>(), new ArrayList<>(), sendMailConfigBean);
    }

    /**
     * This method runs the SQL scripts before every test method to reset the
     * database.
     *
     * @throws ClassNotFoundException
     */
    @Before
    public void seedDatabase() throws ClassNotFoundException {
        LOG.info("@Before seeding the db");

        //putting all SQL table scripts 
        List<String> sqlScripts = new ArrayList<String>();
        sqlScripts.add(loadAsString("createFolderTable.sql"));
        sqlScripts.add(loadAsString("createEmailAddressesTable.sql"));
        sqlScripts.add(loadAsString("createEmailTable.sql"));
        sqlScripts.add(loadAsString("createAttachmentsTable.sql"));
        sqlScripts.add(loadAsString("createEmailToAddressesTable.sql"));

        Class.forName("com.mysql.cj.jdbc.Driver");

        try ( Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);) {
            //for each script, get statements one by one and run them.
            for (String script : sqlScripts) {
                for (String statement : splitStatements(new StringReader(script), ";")) {
                    connection.prepareStatement(statement).execute();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * Loads the given file to a string format
     *
     * @param path
     * @return
     */
    private String loadAsString(final String path) {
        try ( InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);  Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    /**
     * Gets a statement from an SQL file by getting each line, checking if its
     * not a comment and has the statementDelimiter at the end. Returns all the
     * statements from a single SQL file
     *
     * @param reader
     * @param statementDelimiter
     * @return
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                //ignore comment lines and skipped lines
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                //make sure the line is a statement.
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    /**
     * Used to determine if a line is a comment by checking if there are "--" or
     * "//" in the given String.
     *
     * @param line
     * @return True if line is a comment.
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
