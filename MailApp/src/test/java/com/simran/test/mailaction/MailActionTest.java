
package com.simran.test.mailaction;

import com.simran.business.SendAndReceive;
import com.simran.exceptions.InvalidBCCAddressException;
import com.simran.exceptions.InvalidCCAddressException;
import com.simran.exceptions.InvalidMailConfigBeanException;
import com.simran.exceptions.InvalidToAddressException;
import com.simran.exceptions.NoEmailsProvidedException;
import com.simran.fxbeans.MailConfigBean;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import jodd.mail.Email;
import jodd.mail.ReceivedEmail;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class tests the sending and receiving of emails between  Gmail accounts
 * @author Simranjot Kaur Khera
 */
@Ignore
public class MailActionTest {

    private final static Logger LOG = LoggerFactory.getLogger(MailActionTest.class);
    MailConfigBean sendMailConfigBean;
    MailConfigBean receiveConfigBean;
    List<String> ccAddresses;
    List<String> bccAddresses;
    List<String> toAddresses;
    List<File> regAttachments;
    List<File> embAttachments;
    String subject;
    String textMessage;
    String htmlMessage;
    SendAndReceive actionObject;


    /**
     * This method sets up the fields needed for email retrieval and transfer
     * Called before every test
     */
    @Before
    public void setUp() {

        this.sendMailConfigBean = new MailConfigBean("imap.gmail.com", "firstuseracc1@gmail.com", "firstuserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "","");
        this.receiveConfigBean = new MailConfigBean("imap.gmail.com", "seconduseracc2@gmail.com", "seconduserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "","");
        this.ccAddresses = new ArrayList<>();
        this.bccAddresses = new ArrayList<>();
        this.toAddresses = new ArrayList<>();
        this.embAttachments = new ArrayList<>();
        this.regAttachments = new ArrayList<>();
        this.actionObject = new SendAndReceive();
        this.subject = "";
        this.textMessage = "";
        this.htmlMessage = "";

    }
    
    /**
     * This tests for the occasion when there is no email provided 
     * while sending an email
     * @throws InvalidBCCAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidToAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test(expected = NoEmailsProvidedException.class)
    public void testNoAddresses() throws InvalidBCCAddressException, InvalidCCAddressException, InvalidToAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        sendEmailForTesting();
    }

    /**
     * This method tests that emails are received by all accounts in lists provided,
     * and that the emails match.
     * @throws InvalidBCCAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidToAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testReceiveEmail() throws InvalidBCCAddressException, InvalidCCAddressException, InvalidToAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        //setting different lists
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.ccAddresses.add("thirduseracc3@gmail.com");
        this.bccAddresses.add("fourthuseracc4@gmail.com");
        this.subject = "test Receive Email";

        //setting all emails from all accounts to read
        setEmailsToRead();
        //Sending 1 email to all accounts 
        Email sentEmail = sendEmailForTesting();
        //sleep before retrieving emails
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
        }
        //Getting all emails from all accounts
        ReceivedEmail[] receivedFromTo = this.actionObject.receiveEmail(this.receiveConfigBean);
        ReceivedEmail[] receivedFromCC = this.actionObject.receiveEmail(new MailConfigBean("imap.gmail.com", "thirduseracc3@gmail.com", "thirduserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "",""));
        ReceivedEmail[] receivedFromBCC = this.actionObject.receiveEmail(new MailConfigBean("imap.gmail.com", "fourthuseracc4@gmail.com", "fourthuserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "",""));
        //checking if all received same email
        assertTrue(checkForSameEmails(receivedFromTo, receivedFromCC, receivedFromBCC));
        
    }

    /**
     * This method tests for the occasion of an invalid maiconfigbean while receiving
     * @throws InvalidMailConfigBeanException 
     */
    @Test(expected = InvalidMailConfigBeanException.class)
    public void testInvalidMailConfigBeanExceptionForReceive() throws InvalidMailConfigBeanException {
        MailConfigBean faultyBean = new MailConfigBean("imap.gmail.com", "kjbfkb", "", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "","");

        ReceivedEmail[] receivedEmail = this.actionObject.receiveEmail(faultyBean);

    }

    /**
     * Testing that once 5 emails are sent, the user receives 5 emails.
     * Makes sure users gets all emails
     * @throws InvalidMailConfigBeanException
     * @throws InvalidBCCAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidToAddressException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testReceiveEmailFiveEmails() throws InvalidMailConfigBeanException, InvalidBCCAddressException, InvalidCCAddressException, InvalidToAddressException, NoEmailsProvidedException {
        this.actionObject.receiveEmail(this.receiveConfigBean);

        setEmailsToRead();
        sendFiveEmailsForTest();

        ReceivedEmail[] receivedEmails = this.actionObject.receiveEmail(this.receiveConfigBean);
        assertEquals(5, receivedEmails.length);
    }

    /**
     *  Testing for occasion when the mailconfigbean is invalid while sending.
     * @throws InvalidBCCAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidToAddressException
     * @throws NoEmailsProvidedException
     * @throws InvalidMailConfigBeanException 
     */
    @Test(expected = InvalidMailConfigBeanException.class)
    public void testInvalidMailConfigBeanExceptionForSend() throws InvalidBCCAddressException, InvalidCCAddressException, InvalidToAddressException, NoEmailsProvidedException, InvalidMailConfigBeanException {
        MailConfigBean faultyBean = new MailConfigBean("imap.gmail.com", "djbvsbvf", "fourthuserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "","");

        Email sentEmail = this.actionObject.sendEmail(this.toAddresses, this.ccAddresses, this.bccAddresses, this.subject, this.textMessage, this.htmlMessage,
                this.regAttachments, this.embAttachments, faultyBean);
    }

    /**
     * Testing with an invalid to address, custom exception thrown
     * @throws InvalidBCCAddressException
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test(expected = InvalidToAddressException.class)
    public void testInvalidToAddressException() throws InvalidBCCAddressException, InvalidToAddressException, InvalidCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("ahrjbfk.sjfkfnkrfrkj");
        Email sentEmail = sendEmailForTesting();
    }

    /**
     * Testing for invalid cc address, custom exception thrown
     * @throws InvalidCCAddressException
     * @throws InvalidToAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test(expected = InvalidCCAddressException.class)
    public void testInvalidCCAddressException() throws InvalidCCAddressException, InvalidToAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.ccAddresses.add("ahrjbfk.sjfkfnkrfrkj");
        Email sentEmail = sendEmailForTesting();
    }

    /**
     * Testing for invalid bcc address, custom exception thrown
     * @throws InvalidBCCAddressException
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test(expected = InvalidBCCAddressException.class)
    public void testInvalidBCCAddressException() throws InvalidBCCAddressException, InvalidToAddressException, InvalidCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.bccAddresses.add("hfbfhlebhaljkf");
        Email sentEmail = sendEmailForTesting();
    }

    /**
     * Making sure all to receivers receive their emails
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testSendEmailMultipleTo() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.toAddresses.add("thirduseracc3@gmail.com");
        this.subject = "test SendEmail Multiple To- testSendEmailMultipleTo";

        Email sentEmail = sendEmailForTesting();

        assertTrue(sentEmail.to()[0].toString().equals("seconduseracc2@gmail.com")
                && sentEmail.to()[1].toString().equals("thirduseracc3@gmail.com"));
    }

    /**
     * Test with a to and a bcc
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testSendEmailBCCWithTo() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "test SendEmail With BCC and TO";
        File waddlesImage = new File("waddles.jpg");
        this.embAttachments.add(waddlesImage);

        this.bccAddresses.add("thirduseracc3@gmail.com");
        this.bccAddresses.add("fourthuseracc4@gmail.com");
        Email sentEmail = sendEmailForTesting();
        assertTrue(sentEmail.bcc()[0].toString().equals("thirduseracc3@gmail.com")
                && sentEmail.bcc()[1].toString().equals("fourthuseracc4@gmail.com")
                && sentEmail.to()[0].toString().equals("seconduseracc2@gmail.com"));
    }

    /**
     * Testing with cc and to
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testSendEmailCCWithTo() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "test SendEmail With CC and TO";
        File waddlesImage = new File("waddles.jpg");
        this.embAttachments.add(waddlesImage);

        this.ccAddresses.add("thirduseracc3@gmail.com");
        this.ccAddresses.add("fourthuseracc4@gmail.com");
        Email sentEmail = sendEmailForTesting();

        assertTrue(sentEmail.cc()[0].toString().equals("thirduseracc3@gmail.com")
                && sentEmail.cc()[1].toString().equals("fourthuseracc4@gmail.com")
                && sentEmail.to()[0].toString().equals("seconduseracc2@gmail.com"));

    }

    /**
     * Sends an email with subject, text and 1 to
     * A basic email
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testSendEmailWithBasicInfo() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "test SendEmail With Basic Info()";
        this.textMessage = "Text Message here";

        Email sentEmail = sendEmailForTesting();

        //Checking for from,to and subject in email object to be the same as assigned
        sentEmail.messages().stream().map((msg) -> {
            if (msg.getMimeType().equals("TEXT/PLAIN")) {

                assertTrue(msg.getContent().equals(this.htmlMessage)
                        && sentEmail.to()[0].toString().equals(this.toAddresses.get(0))
                        && sentEmail.subject().equals(this.subject));
            }
            return msg;
        });
    }

    /**
     * Tests with embedded attachments
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testSendEmailWithEmbeddedAttachments() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "text SendEmail With Embedded Attachments";

        File waddlesImage = new File("waddles.jpg");
        File kenImage = new File("FreeFall.jpg");

        this.embAttachments.add(waddlesImage);
        this.embAttachments.add(kenImage);

        Email sentEmail = sendEmailForTesting();

        assertTrue(sentEmail.attachments().get(0).getName().equals(waddlesImage.getName())
                && sentEmail.attachments().get(1).getName().equals(kenImage.getName()));

    }

    /**
     * Testing with regular attachments
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    @Test
    public void testSendEmailWithRegAttachments() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "test SendEmail With Attachments- testSendEmailWithRegAttachments";

        File kenFlyingImage = new File("FreeFall.jpg");
        File kenImage = new File("WindsorKen180.jpg");

        this.regAttachments.add(kenFlyingImage);
        this.regAttachments.add(kenImage);

        Email sentEmail = sendEmailForTesting();

        assertTrue(sentEmail.attachments().get(0).getName().equals(kenFlyingImage.getName())
                && sentEmail.attachments().get(1).getName().equals(kenImage.getName()));

    }

    /**
     * 
     * Testing with html messages
     */
    @Test
    public void testSendEmailWithHtmlMessage() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "test SendEmail With Html Message- testSendEmailWithHtmlMessage";
        this.htmlMessage = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img src='cid:FreeFall.jpg'>"
                + "<h2>I'm flying!</h2></body></html>";
        Email sentEmail = sendEmailForTesting();

        sentEmail.messages().stream().map((msg) -> {
            if (msg.getMimeType().equals("TEXT/HTML")) {
                assertEquals(msg.getContent(), this.htmlMessage);
            }
            return msg;
        });
    }

    /**
     * This private method helps send emails with all the changes made in the test methods
     * @return
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    private Email sendEmailForTesting() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        return this.actionObject.sendEmail(this.toAddresses, this.ccAddresses, this.bccAddresses, this.subject, this.textMessage, this.htmlMessage,
                this.regAttachments, this.embAttachments, this.sendMailConfigBean);
    }

    /**
     * This method helps send 5 emails to one user for a test
     * @throws InvalidToAddressException
     * @throws InvalidCCAddressException
     * @throws InvalidBCCAddressException
     * @throws InvalidMailConfigBeanException
     * @throws NoEmailsProvidedException 
     */
    private void sendFiveEmailsForTest() throws InvalidToAddressException, InvalidCCAddressException, InvalidBCCAddressException, InvalidMailConfigBeanException, NoEmailsProvidedException {
        this.toAddresses.add("seconduseracc2@gmail.com");
        this.subject = "Testing For 5 emails";

        for (int i = 0; i < 5; ++i) {
            this.actionObject.sendEmail(this.toAddresses, this.ccAddresses, this.bccAddresses, this.subject, this.textMessage, this.htmlMessage,
                    this.regAttachments, this.embAttachments, this.sendMailConfigBean);
        }
        //sleep before trieveing
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
        }
    }

    /**
     * This method sets all account emails to read, used before sending and retrieving emails
     * This helps make sure that the receiveEmails method receives the ones last sent
     * @throws InvalidMailConfigBeanException 
     */
    private void setEmailsToRead() throws InvalidMailConfigBeanException {
        this.actionObject.receiveEmail(this.receiveConfigBean);
        this.actionObject.receiveEmail(new MailConfigBean("imap.gmail.com", "thirduseracc3@gmail.com", "thirduserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "",""));
        this.actionObject.receiveEmail(new MailConfigBean("imap.gmail.com", "fourthuseracc4@gmail.com", "fourthuserpassword", "imap.gmail.com",
                "smtp.gmail.com", "993", "465", "", "", "3306", "",""));
    }

    /**
     * This method compares emails to make sure that all users got the same email with cc, bcc and to
     * @param fromTo
     * @param fromCC
     * @param fromBCC
     * @return 
     */
    private boolean checkForSameEmails(ReceivedEmail[] fromTo, ReceivedEmail[] fromCC, ReceivedEmail[] fromBCC) {
        if (fromTo.length != 0 && fromCC.length != 0 && fromBCC.length != 0) {
            if (fromTo[0].subject().equals(fromCC[0].subject())) {
                if (fromCC[0].subject().equals(fromBCC[0].subject())) {
                    return true;
                }
            }
        }
        return false;
    }
}
