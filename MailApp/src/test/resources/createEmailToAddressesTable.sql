

 -- This is a bridging table used for email and emailAddresses. 
 -- It makes it possible for an email to be sent to multiple emailAddresses. 
 -- It also categorizes the addresses based on if it is a TO, CC or BCC address. 
 -- This information is stored in the EMAILTYPE field. 
 -- Author:  Simranjot Kaur Khera
 -- Created: Sep. 30, 2020
 

USE EMAILAPP;

--turning off database check to drop table with constraints
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS EMAILTOADDRESSES;

--turning database checks back on
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE EMAILTOADDRESSES(
    EMAILID int,
    ADDRESSID int,
    EMAILTYPE varchar(3) NOT NULL default '',
    PRIMARY KEY(EMAILID,ADDRESSID),
    CONSTRAINT fkemailid FOREIGN KEY (EMAILID) REFERENCES EMAILS(EMAILID),
    CONSTRAINT fkaddressid FOREIGN KEY (ADDRESSID) REFERENCES EMAILADDRESSES(ADDRESSID)

);

INSERT INTO EMAILTOADDRESSES (EMAILID,ADDRESSID,EMAILTYPE) VALUES
(1,2,"TO"), (2,2,"CC"),(3,1,"TO"),(3,3,"CC"), (4,3,"TO");