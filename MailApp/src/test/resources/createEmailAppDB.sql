/**
 * This script creates the emailapp database which will store
 * email related tables. 
 * Author Simranjot Kaur Khera
 * Created: Sep. 30, 2020  
 */

DROP DATABASE IF EXISTS EMAILAPP;
CREATE DATABASE EMAILAPP;

USE EMAILAPP;

DROP USER IF EXISTS email@localhost;
CREATE USER email@'localhost' IDENTIFIED BY 'kfstandard';
GRANT ALL ON EMAILAPP.* TO email@'localhost';

-- This creates a user with access from any IP number except localhost
-- Use only if your MyQL database is on a different host from localhost
-- DROP USER IF EXISTS email; -- % user
-- CREATE USER email IDENTIFIED BY 'kfstandard';
--GRANT ALL ON EMAILAPP TO email@'%';

FLUSH PRIVILEGES;
