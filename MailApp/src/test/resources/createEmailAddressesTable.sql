
 --EmailAddresses contains all emails used by this programs, 
 --no duplications. These addresses are used in the EmailToAddresses table
 --where they are linked to an email as To, CC or BCC. 
 --Author:  Simranjot Kaur Khera
 -- Created: Sep. 30, 2020
 

USE EMAILAPP;

--turning off database check to drop table with constraints
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS EMAILADDRESSES;

--turning database checks back on
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE EMAILADDRESSES(
    ADDRESSID int NOT NULL auto_increment,
    ADDRESS varchar(320) NOT NULL default '',
    PRIMARY KEY (ADDRESSID)

);

--Test Data
INSERT INTO EMAILADDRESSES (ADDRESS) VALUES
('firstuseracc1@gmail.com'),('seconduseracc2@gmail.com'),('thirduseracc3@gmail.com');