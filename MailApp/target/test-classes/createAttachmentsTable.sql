
 -- Attachments table contains all attachments (normal and embedded)
 -- the CID field is used to differentiate between types of attachments.
 -- Author:  Simranjot Kaur Khera
 -- Created: Sep. 30, 2020
 

USE EMAILAPP;

--turning off database check to drop table with constraints
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS ATTACHMENTS;

--turning database checks back on
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE ATTACHMENTS(
    EMAILID int,
    ATTACHMENTID int NOT NULL auto_increment,
    FILENAME varchar(255) default '',
    ATTACHMENTBLOB mediumblob,
    CID varchar(128),
    PRIMARY KEY (ATTACHMENTID),
    CONSTRAINT fkemailidattachments FOREIGN KEY (EMAILID) REFERENCES EMAILS(EMAILID)
)ENGINE=InnoDB;

INSERT INTO ATTACHMENTS (EMAILID,FILENAME,ATTACHMENTBLOB,CID) VALUES
(1,"waddles.jpg",null,"waddles.jpg"), (4, "FreeFall.jpg", null, "FreeFall.jpg");