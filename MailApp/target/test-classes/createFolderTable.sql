 -- Folder table contains names of all folders on the mail app.
 -- Starts with Sent, Draft and Inbox. 
 --Author:  Simranjot Kaur Khera
 --Created: Oct. 1, 2020
 

--using emailapp database
USE EMAILAPP;
--turning off database check to drop table with constraints
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS FOLDERS;

--turning database checks back on
SET FOREIGN_KEY_CHECKS = 1;

--creating folder table with auto incremented primary key. 
CREATE TABLE FOLDERS(
    FOLDERID int NOT NULL auto_increment,
    FOLDERNAME varchar(255) NOT NULL default '',
    PRIMARY KEY(FolderID)
);

--Setting starting folders.
INSERT INTO FOLDERS (FOLDERNAME) VALUES ('SENT'), ('DRAFT'),('INBOX');