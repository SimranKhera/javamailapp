

 -- The email table will store both sent and received emails.
 --To differentiate between them we use the folderID as a foreign key.
 --It contains general information about an email that we will be presenting on the GUI. 
 -- Author:  Simranjot Kaur Khera
 -- Created: Sep. 30, 2020
 

USE EMAILAPP;

--turning off database check to drop table with conSET FOREIGN_KEY_CHECKS = 0;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS EMAILS;

--turning database checks back on
SET FOREIGN_KEY_CHECKS = 1;


CREATE TABLE EMAILS(
    EMAILID int NOT NULL auto_increment,
    FOLDERID int,
    FROMADDRESS varchar(320) NOT NULL,
    SUBJECT varchar(70) NOT NULL default '',
    TEXTMESSAGE TEXT ,
    HTMLMESSAGE TEXT ,
    SENTDATE timestamp,
    RECEIVEDDATE timestamp,
    PRIMARY KEY (EMAILID),
    CONSTRAINT fkfolderid_emails FOREIGN KEY (FOLDERID) REFERENCES FOLDERS(FOLDERID)
);

INSERT INTO EMAILS (FOLDERID,FROMADDRESS,SUBJECT,TEXTMESSAGE,HTMLMESSAGE,SENTDATE,RECEIVEDDATE) VALUES
(1,"firstuseracc1@gmail.com","sentEmail 1", "Email 1, textMessage in sent","",CURRENT_TIMESTAMP, null),
(2, "firstuseracc1@gmail.com","draft email 1", "Email 2, textMessage in draft","",null,null),
(3, "seconduseracc2@gmail.com", "received email 1", "Email 3, textMessage in inbox","",CURRENT_TIMESTAMP,CURRENT_TIMESTAMP),
(3, "firstuseracc1@gmail.com", "received email 2", "Email 4, textMessage in inbox","",CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
