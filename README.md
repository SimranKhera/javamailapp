# JavaMailApp

This is an email app that introduces the concept of beans and controllers. 
Here we have used JavaFx and SQL scripts. 
We have also learnt and implemented more efficient unit tests with best practices.

Achieved an application capable of sending emails with or without attachments. Emails can also be CCed or BCCed. 
